
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/scripts/Game');
require('./assets/scripts/Player');
require('./assets/scripts/ScoreAnim');
require('./assets/scripts/ScoreFX');
require('./assets/scripts/Star');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Player.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '66a90Izn7xHUKj+YyfZq2II', 'Player');
// scripts/Player.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    // 主角跳跃高度
    jumpHeight: 0,
    // 主角跳跃持续时间
    jumpDuration: 0,
    // 辅助形变动作时间
    squashDuration: 0,
    // 最大移动速度
    maxMoveSpeed: 0,
    // 加速度
    accel: 0,
    // 跳跃音效资源
    jumpAudio: {
      "default": null,
      type: cc.AudioClip
    }
  },
  // use this for initialization
  onLoad: function onLoad() {
    this.enabled = false; // 加速度方向开关

    this.accLeft = false;
    this.accRight = false; // 主角当前水平方向速度

    this.xSpeed = 0; // screen boundaries

    this.minPosX = -this.node.parent.width / 2;
    this.maxPosX = this.node.parent.width / 2; // 初始化键盘输入监听

    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    var touchReceiver = cc.Canvas.instance.node;
    touchReceiver.on('touchstart', this.onTouchStart, this);
    touchReceiver.on('touchend', this.onTouchEnd, this);
  },
  onDestroy: function onDestroy() {
    // 取消键盘输入监听
    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    var touchReceiver = cc.Canvas.instance.node;
    touchReceiver.off('touchstart', this.onTouchStart, this);
    touchReceiver.off('touchend', this.onTouchEnd, this);
  },
  onKeyDown: function onKeyDown(event) {
    switch (event.keyCode) {
      case cc.macro.KEY.a:
      case cc.macro.KEY.left:
        this.accLeft = true;
        this.accRight = false;
        break;

      case cc.macro.KEY.d:
      case cc.macro.KEY.right:
        this.accLeft = false;
        this.accRight = true;
        break;
    }
  },
  onKeyUp: function onKeyUp(event) {
    switch (event.keyCode) {
      case cc.macro.KEY.a:
      case cc.macro.KEY.left:
        this.accLeft = false;
        break;

      case cc.macro.KEY.d:
      case cc.macro.KEY.right:
        this.accRight = false;
        break;
    }
  },
  onTouchStart: function onTouchStart(event) {
    var touchLoc = event.getLocation();

    if (touchLoc.x >= cc.winSize.width / 2) {
      this.accLeft = false;
      this.accRight = true;
    } else {
      this.accLeft = true;
      this.accRight = false;
    }
  },
  onTouchEnd: function onTouchEnd(event) {
    this.accLeft = false;
    this.accRight = false;
  },
  runJumpAction: function runJumpAction() {
    // 跳跃上升
    var jumpUp = cc.tween().by(this.jumpDuration, {
      y: this.jumpHeight
    }, {
      easing: 'sineOut'
    }); // 下落

    var jumpDown = cc.tween().by(this.jumpDuration, {
      y: -this.jumpHeight
    }, {
      easing: 'sineIn'
    }); // 形变

    var squash = cc.tween().to(this.squashDuration, {
      scaleX: 1,
      scaleY: 0.6
    });
    var stretch = cc.tween().to(this.squashDuration, {
      scaleX: 1,
      scaleY: 1.2
    });
    var scaleBack = cc.tween().to(this.squashDuration, {
      scaleX: 1,
      scaleY: 1
    }); // 创建一个缓动

    var tween = cc.tween() // 按顺序执行动作
    .sequence(squash, stretch, jumpUp, scaleBack, jumpDown) // 添加一个回调函数，在前面的动作都结束时调用我们定义的 playJumpSound() 方法
    .call(this.playJumpSound, this); // 不断重复

    return cc.tween().repeatForever(tween);
  },
  playJumpSound: function playJumpSound() {
    // 调用声音引擎播放声音
    cc.audioEngine.playEffect(this.jumpAudio, false);
  },
  getCenterPos: function getCenterPos() {
    var centerPos = cc.v2(this.node.x, this.node.y + this.node.height / 2);
    return centerPos;
  },
  startMoveAt: function startMoveAt(pos) {
    this.enabled = true;
    this.xSpeed = 0;
    this.node.setPosition(pos);
    var jumpAction = this.runJumpAction();
    cc.tween(this.node).then(jumpAction).start();
  },
  stopMove: function stopMove() {
    this.node.stopAllActions();
  },
  // called every frame
  update: function update(dt) {
    // 根据当前加速度方向每帧更新速度
    if (this.accLeft) {
      this.xSpeed -= this.accel * dt;
    } else if (this.accRight) {
      this.xSpeed += this.accel * dt;
    } // 限制主角的速度不能超过最大值


    if (Math.abs(this.xSpeed) > this.maxMoveSpeed) {
      // if speed reach limit, use max speed with current direction
      this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed);
    } // 根据当前速度更新主角的位置


    this.node.x += this.xSpeed * dt; // limit player position inside screen

    if (this.node.x > this.node.parent.width / 2) {
      this.node.x = this.node.parent.width / 2;
      this.xSpeed = 0;
    } else if (this.node.x < -this.node.parent.width / 2) {
      this.node.x = -this.node.parent.width / 2;
      this.xSpeed = 0;
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcUGxheWVyLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwianVtcEhlaWdodCIsImp1bXBEdXJhdGlvbiIsInNxdWFzaER1cmF0aW9uIiwibWF4TW92ZVNwZWVkIiwiYWNjZWwiLCJqdW1wQXVkaW8iLCJ0eXBlIiwiQXVkaW9DbGlwIiwib25Mb2FkIiwiZW5hYmxlZCIsImFjY0xlZnQiLCJhY2NSaWdodCIsInhTcGVlZCIsIm1pblBvc1giLCJub2RlIiwicGFyZW50Iiwid2lkdGgiLCJtYXhQb3NYIiwic3lzdGVtRXZlbnQiLCJvbiIsIlN5c3RlbUV2ZW50IiwiRXZlbnRUeXBlIiwiS0VZX0RPV04iLCJvbktleURvd24iLCJLRVlfVVAiLCJvbktleVVwIiwidG91Y2hSZWNlaXZlciIsIkNhbnZhcyIsImluc3RhbmNlIiwib25Ub3VjaFN0YXJ0Iiwib25Ub3VjaEVuZCIsIm9uRGVzdHJveSIsIm9mZiIsImV2ZW50Iiwia2V5Q29kZSIsIm1hY3JvIiwiS0VZIiwiYSIsImxlZnQiLCJkIiwicmlnaHQiLCJ0b3VjaExvYyIsImdldExvY2F0aW9uIiwieCIsIndpblNpemUiLCJydW5KdW1wQWN0aW9uIiwianVtcFVwIiwidHdlZW4iLCJieSIsInkiLCJlYXNpbmciLCJqdW1wRG93biIsInNxdWFzaCIsInRvIiwic2NhbGVYIiwic2NhbGVZIiwic3RyZXRjaCIsInNjYWxlQmFjayIsInNlcXVlbmNlIiwiY2FsbCIsInBsYXlKdW1wU291bmQiLCJyZXBlYXRGb3JldmVyIiwiYXVkaW9FbmdpbmUiLCJwbGF5RWZmZWN0IiwiZ2V0Q2VudGVyUG9zIiwiY2VudGVyUG9zIiwidjIiLCJoZWlnaHQiLCJzdGFydE1vdmVBdCIsInBvcyIsInNldFBvc2l0aW9uIiwianVtcEFjdGlvbiIsInRoZW4iLCJzdGFydCIsInN0b3BNb3ZlIiwic3RvcEFsbEFjdGlvbnMiLCJ1cGRhdGUiLCJkdCIsIk1hdGgiLCJhYnMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSO0FBQ0FDLElBQUFBLFVBQVUsRUFBRSxDQUZKO0FBR1I7QUFDQUMsSUFBQUEsWUFBWSxFQUFFLENBSk47QUFLUjtBQUNBQyxJQUFBQSxjQUFjLEVBQUUsQ0FOUjtBQU9SO0FBQ0FDLElBQUFBLFlBQVksRUFBRSxDQVJOO0FBU1I7QUFDQUMsSUFBQUEsS0FBSyxFQUFFLENBVkM7QUFXUjtBQUNBQyxJQUFBQSxTQUFTLEVBQUU7QUFDUCxpQkFBUyxJQURGO0FBRVBDLE1BQUFBLElBQUksRUFBRVYsRUFBRSxDQUFDVztBQUZGO0FBWkgsR0FIUDtBQXFCTDtBQUNBQyxFQUFBQSxNQUFNLEVBQUUsa0JBQVk7QUFDaEIsU0FBS0MsT0FBTCxHQUFlLEtBQWYsQ0FEZ0IsQ0FFaEI7O0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEtBQWY7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLEtBQWhCLENBSmdCLENBS2hCOztBQUNBLFNBQUtDLE1BQUwsR0FBYyxDQUFkLENBTmdCLENBT2hCOztBQUNBLFNBQUtDLE9BQUwsR0FBZSxDQUFDLEtBQUtDLElBQUwsQ0FBVUMsTUFBVixDQUFpQkMsS0FBbEIsR0FBd0IsQ0FBdkM7QUFDQSxTQUFLQyxPQUFMLEdBQWUsS0FBS0gsSUFBTCxDQUFVQyxNQUFWLENBQWlCQyxLQUFqQixHQUF1QixDQUF0QyxDQVRnQixDQVdoQjs7QUFDQXBCLElBQUFBLEVBQUUsQ0FBQ3NCLFdBQUgsQ0FBZUMsRUFBZixDQUFrQnZCLEVBQUUsQ0FBQ3dCLFdBQUgsQ0FBZUMsU0FBZixDQUF5QkMsUUFBM0MsRUFBcUQsS0FBS0MsU0FBMUQsRUFBcUUsSUFBckU7QUFDQTNCLElBQUFBLEVBQUUsQ0FBQ3NCLFdBQUgsQ0FBZUMsRUFBZixDQUFrQnZCLEVBQUUsQ0FBQ3dCLFdBQUgsQ0FBZUMsU0FBZixDQUF5QkcsTUFBM0MsRUFBbUQsS0FBS0MsT0FBeEQsRUFBaUUsSUFBakU7QUFFQSxRQUFJQyxhQUFhLEdBQUc5QixFQUFFLENBQUMrQixNQUFILENBQVVDLFFBQVYsQ0FBbUJkLElBQXZDO0FBQ0FZLElBQUFBLGFBQWEsQ0FBQ1AsRUFBZCxDQUFpQixZQUFqQixFQUErQixLQUFLVSxZQUFwQyxFQUFrRCxJQUFsRDtBQUNBSCxJQUFBQSxhQUFhLENBQUNQLEVBQWQsQ0FBaUIsVUFBakIsRUFBNkIsS0FBS1csVUFBbEMsRUFBOEMsSUFBOUM7QUFDSCxHQXhDSTtBQTBDTEMsRUFBQUEsU0ExQ0ssdUJBMENRO0FBQ1Q7QUFDQW5DLElBQUFBLEVBQUUsQ0FBQ3NCLFdBQUgsQ0FBZWMsR0FBZixDQUFtQnBDLEVBQUUsQ0FBQ3dCLFdBQUgsQ0FBZUMsU0FBZixDQUF5QkMsUUFBNUMsRUFBc0QsS0FBS0MsU0FBM0QsRUFBc0UsSUFBdEU7QUFDQTNCLElBQUFBLEVBQUUsQ0FBQ3NCLFdBQUgsQ0FBZWMsR0FBZixDQUFtQnBDLEVBQUUsQ0FBQ3dCLFdBQUgsQ0FBZUMsU0FBZixDQUF5QkcsTUFBNUMsRUFBb0QsS0FBS0MsT0FBekQsRUFBa0UsSUFBbEU7QUFFQSxRQUFJQyxhQUFhLEdBQUc5QixFQUFFLENBQUMrQixNQUFILENBQVVDLFFBQVYsQ0FBbUJkLElBQXZDO0FBQ0FZLElBQUFBLGFBQWEsQ0FBQ00sR0FBZCxDQUFrQixZQUFsQixFQUFnQyxLQUFLSCxZQUFyQyxFQUFtRCxJQUFuRDtBQUNBSCxJQUFBQSxhQUFhLENBQUNNLEdBQWQsQ0FBa0IsVUFBbEIsRUFBOEIsS0FBS0YsVUFBbkMsRUFBK0MsSUFBL0M7QUFDSCxHQWxESTtBQW9ETFAsRUFBQUEsU0FwREsscUJBb0RNVSxLQXBETixFQW9EYTtBQUNkLFlBQU9BLEtBQUssQ0FBQ0MsT0FBYjtBQUNJLFdBQUt0QyxFQUFFLENBQUN1QyxLQUFILENBQVNDLEdBQVQsQ0FBYUMsQ0FBbEI7QUFDQSxXQUFLekMsRUFBRSxDQUFDdUMsS0FBSCxDQUFTQyxHQUFULENBQWFFLElBQWxCO0FBQ0ksYUFBSzVCLE9BQUwsR0FBZSxJQUFmO0FBQ0EsYUFBS0MsUUFBTCxHQUFnQixLQUFoQjtBQUNBOztBQUNKLFdBQUtmLEVBQUUsQ0FBQ3VDLEtBQUgsQ0FBU0MsR0FBVCxDQUFhRyxDQUFsQjtBQUNBLFdBQUszQyxFQUFFLENBQUN1QyxLQUFILENBQVNDLEdBQVQsQ0FBYUksS0FBbEI7QUFDSSxhQUFLOUIsT0FBTCxHQUFlLEtBQWY7QUFDQSxhQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0E7QUFWUjtBQVlILEdBakVJO0FBbUVMYyxFQUFBQSxPQW5FSyxtQkFtRUlRLEtBbkVKLEVBbUVXO0FBQ1osWUFBT0EsS0FBSyxDQUFDQyxPQUFiO0FBQ0ksV0FBS3RDLEVBQUUsQ0FBQ3VDLEtBQUgsQ0FBU0MsR0FBVCxDQUFhQyxDQUFsQjtBQUNBLFdBQUt6QyxFQUFFLENBQUN1QyxLQUFILENBQVNDLEdBQVQsQ0FBYUUsSUFBbEI7QUFDSSxhQUFLNUIsT0FBTCxHQUFlLEtBQWY7QUFDQTs7QUFDSixXQUFLZCxFQUFFLENBQUN1QyxLQUFILENBQVNDLEdBQVQsQ0FBYUcsQ0FBbEI7QUFDQSxXQUFLM0MsRUFBRSxDQUFDdUMsS0FBSCxDQUFTQyxHQUFULENBQWFJLEtBQWxCO0FBQ0ksYUFBSzdCLFFBQUwsR0FBZ0IsS0FBaEI7QUFDQTtBQVJSO0FBVUgsR0E5RUk7QUFnRkxrQixFQUFBQSxZQWhGSyx3QkFnRlNJLEtBaEZULEVBZ0ZnQjtBQUNqQixRQUFJUSxRQUFRLEdBQUdSLEtBQUssQ0FBQ1MsV0FBTixFQUFmOztBQUNBLFFBQUlELFFBQVEsQ0FBQ0UsQ0FBVCxJQUFjL0MsRUFBRSxDQUFDZ0QsT0FBSCxDQUFXNUIsS0FBWCxHQUFpQixDQUFuQyxFQUFzQztBQUNsQyxXQUFLTixPQUFMLEdBQWUsS0FBZjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsSUFBaEI7QUFDSCxLQUhELE1BR087QUFDSCxXQUFLRCxPQUFMLEdBQWUsSUFBZjtBQUNBLFdBQUtDLFFBQUwsR0FBZ0IsS0FBaEI7QUFDSDtBQUNKLEdBekZJO0FBMkZMbUIsRUFBQUEsVUEzRkssc0JBMkZPRyxLQTNGUCxFQTJGYztBQUNmLFNBQUt2QixPQUFMLEdBQWUsS0FBZjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsS0FBaEI7QUFDSCxHQTlGSTtBQWdHTGtDLEVBQUFBLGFBQWEsRUFBRSx5QkFBWTtBQUN2QjtBQUNBLFFBQUlDLE1BQU0sR0FBR2xELEVBQUUsQ0FBQ21ELEtBQUgsR0FBV0MsRUFBWCxDQUFjLEtBQUsvQyxZQUFuQixFQUFpQztBQUFFZ0QsTUFBQUEsQ0FBQyxFQUFFLEtBQUtqRDtBQUFWLEtBQWpDLEVBQXlEO0FBQUVrRCxNQUFBQSxNQUFNLEVBQUU7QUFBVixLQUF6RCxDQUFiLENBRnVCLENBR3ZCOztBQUNBLFFBQUlDLFFBQVEsR0FBR3ZELEVBQUUsQ0FBQ21ELEtBQUgsR0FBV0MsRUFBWCxDQUFjLEtBQUsvQyxZQUFuQixFQUFpQztBQUFFZ0QsTUFBQUEsQ0FBQyxFQUFFLENBQUMsS0FBS2pEO0FBQVgsS0FBakMsRUFBMEQ7QUFBRWtELE1BQUFBLE1BQU0sRUFBRTtBQUFWLEtBQTFELENBQWYsQ0FKdUIsQ0FLdkI7O0FBQ0EsUUFBSUUsTUFBTSxHQUFHeEQsRUFBRSxDQUFDbUQsS0FBSCxHQUFXTSxFQUFYLENBQWMsS0FBS25ELGNBQW5CLEVBQW1DO0FBQUVvRCxNQUFBQSxNQUFNLEVBQUUsQ0FBVjtBQUFhQyxNQUFBQSxNQUFNLEVBQUU7QUFBckIsS0FBbkMsQ0FBYjtBQUNBLFFBQUlDLE9BQU8sR0FBRzVELEVBQUUsQ0FBQ21ELEtBQUgsR0FBV00sRUFBWCxDQUFjLEtBQUtuRCxjQUFuQixFQUFtQztBQUFFb0QsTUFBQUEsTUFBTSxFQUFFLENBQVY7QUFBYUMsTUFBQUEsTUFBTSxFQUFFO0FBQXJCLEtBQW5DLENBQWQ7QUFDQSxRQUFJRSxTQUFTLEdBQUc3RCxFQUFFLENBQUNtRCxLQUFILEdBQVdNLEVBQVgsQ0FBYyxLQUFLbkQsY0FBbkIsRUFBbUM7QUFBRW9ELE1BQUFBLE1BQU0sRUFBRSxDQUFWO0FBQWFDLE1BQUFBLE1BQU0sRUFBRTtBQUFyQixLQUFuQyxDQUFoQixDQVJ1QixDQVV2Qjs7QUFDQSxRQUFJUixLQUFLLEdBQUduRCxFQUFFLENBQUNtRCxLQUFILEdBQ0k7QUFESixLQUVLVyxRQUZMLENBRWNOLE1BRmQsRUFFc0JJLE9BRnRCLEVBRStCVixNQUYvQixFQUV1Q1csU0FGdkMsRUFFa0ROLFFBRmxELEVBR0k7QUFISixLQUlLUSxJQUpMLENBSVUsS0FBS0MsYUFKZixFQUk4QixJQUo5QixDQUFaLENBWHVCLENBaUJ2Qjs7QUFDQSxXQUFPaEUsRUFBRSxDQUFDbUQsS0FBSCxHQUFXYyxhQUFYLENBQXlCZCxLQUF6QixDQUFQO0FBQ0gsR0FuSEk7QUFxSExhLEVBQUFBLGFBQWEsRUFBRSx5QkFBWTtBQUN2QjtBQUNBaEUsSUFBQUEsRUFBRSxDQUFDa0UsV0FBSCxDQUFlQyxVQUFmLENBQTBCLEtBQUsxRCxTQUEvQixFQUEwQyxLQUExQztBQUNILEdBeEhJO0FBMEhMMkQsRUFBQUEsWUFBWSxFQUFFLHdCQUFZO0FBQ3RCLFFBQUlDLFNBQVMsR0FBR3JFLEVBQUUsQ0FBQ3NFLEVBQUgsQ0FBTSxLQUFLcEQsSUFBTCxDQUFVNkIsQ0FBaEIsRUFBbUIsS0FBSzdCLElBQUwsQ0FBVW1DLENBQVYsR0FBYyxLQUFLbkMsSUFBTCxDQUFVcUQsTUFBVixHQUFpQixDQUFsRCxDQUFoQjtBQUNBLFdBQU9GLFNBQVA7QUFDSCxHQTdISTtBQStITEcsRUFBQUEsV0FBVyxFQUFFLHFCQUFVQyxHQUFWLEVBQWU7QUFDeEIsU0FBSzVELE9BQUwsR0FBZSxJQUFmO0FBQ0EsU0FBS0csTUFBTCxHQUFjLENBQWQ7QUFDQSxTQUFLRSxJQUFMLENBQVV3RCxXQUFWLENBQXNCRCxHQUF0QjtBQUVBLFFBQUlFLFVBQVUsR0FBRyxLQUFLMUIsYUFBTCxFQUFqQjtBQUNBakQsSUFBQUEsRUFBRSxDQUFDbUQsS0FBSCxDQUFTLEtBQUtqQyxJQUFkLEVBQW9CMEQsSUFBcEIsQ0FBeUJELFVBQXpCLEVBQXFDRSxLQUFyQztBQUNILEdBdElJO0FBd0lMQyxFQUFBQSxRQUFRLEVBQUUsb0JBQVk7QUFDbEIsU0FBSzVELElBQUwsQ0FBVTZELGNBQVY7QUFDSCxHQTFJSTtBQTRJTDtBQUNBQyxFQUFBQSxNQUFNLEVBQUUsZ0JBQVVDLEVBQVYsRUFBYztBQUNsQjtBQUNBLFFBQUksS0FBS25FLE9BQVQsRUFBa0I7QUFDZCxXQUFLRSxNQUFMLElBQWUsS0FBS1IsS0FBTCxHQUFheUUsRUFBNUI7QUFDSCxLQUZELE1BRU8sSUFBSSxLQUFLbEUsUUFBVCxFQUFtQjtBQUN0QixXQUFLQyxNQUFMLElBQWUsS0FBS1IsS0FBTCxHQUFheUUsRUFBNUI7QUFDSCxLQU5pQixDQU9sQjs7O0FBQ0EsUUFBS0MsSUFBSSxDQUFDQyxHQUFMLENBQVMsS0FBS25FLE1BQWQsSUFBd0IsS0FBS1QsWUFBbEMsRUFBaUQ7QUFDN0M7QUFDQSxXQUFLUyxNQUFMLEdBQWMsS0FBS1QsWUFBTCxHQUFvQixLQUFLUyxNQUF6QixHQUFrQ2tFLElBQUksQ0FBQ0MsR0FBTCxDQUFTLEtBQUtuRSxNQUFkLENBQWhEO0FBQ0gsS0FYaUIsQ0FhbEI7OztBQUNBLFNBQUtFLElBQUwsQ0FBVTZCLENBQVYsSUFBZSxLQUFLL0IsTUFBTCxHQUFjaUUsRUFBN0IsQ0Fka0IsQ0FnQmxCOztBQUNBLFFBQUssS0FBSy9ELElBQUwsQ0FBVTZCLENBQVYsR0FBYyxLQUFLN0IsSUFBTCxDQUFVQyxNQUFWLENBQWlCQyxLQUFqQixHQUF1QixDQUExQyxFQUE2QztBQUN6QyxXQUFLRixJQUFMLENBQVU2QixDQUFWLEdBQWMsS0FBSzdCLElBQUwsQ0FBVUMsTUFBVixDQUFpQkMsS0FBakIsR0FBdUIsQ0FBckM7QUFDQSxXQUFLSixNQUFMLEdBQWMsQ0FBZDtBQUNILEtBSEQsTUFHTyxJQUFJLEtBQUtFLElBQUwsQ0FBVTZCLENBQVYsR0FBYyxDQUFDLEtBQUs3QixJQUFMLENBQVVDLE1BQVYsQ0FBaUJDLEtBQWxCLEdBQXdCLENBQTFDLEVBQTZDO0FBQ2hELFdBQUtGLElBQUwsQ0FBVTZCLENBQVYsR0FBYyxDQUFDLEtBQUs3QixJQUFMLENBQVVDLE1BQVYsQ0FBaUJDLEtBQWxCLEdBQXdCLENBQXRDO0FBQ0EsV0FBS0osTUFBTCxHQUFjLENBQWQ7QUFDSDtBQUNKO0FBcktJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuIFxyXG4gICAgcHJvcGVydGllczoge1xyXG4gICAgICAgIC8vIOS4u+inkui3s+i3g+mrmOW6plxyXG4gICAgICAgIGp1bXBIZWlnaHQ6IDAsXHJcbiAgICAgICAgLy8g5Li76KeS6Lez6LeD5oyB57ut5pe26Ze0XHJcbiAgICAgICAganVtcER1cmF0aW9uOiAwLFxyXG4gICAgICAgIC8vIOi+heWKqeW9ouWPmOWKqOS9nOaXtumXtFxyXG4gICAgICAgIHNxdWFzaER1cmF0aW9uOiAwLFxyXG4gICAgICAgIC8vIOacgOWkp+enu+WKqOmAn+W6plxyXG4gICAgICAgIG1heE1vdmVTcGVlZDogMCxcclxuICAgICAgICAvLyDliqDpgJ/luqZcclxuICAgICAgICBhY2NlbDogMCxcclxuICAgICAgICAvLyDot7Pot4Ppn7PmlYjotYTmupBcclxuICAgICAgICBqdW1wQXVkaW86IHtcclxuICAgICAgICAgICAgZGVmYXVsdDogbnVsbCxcclxuICAgICAgICAgICAgdHlwZTogY2MuQXVkaW9DbGlwXHJcbiAgICAgICAgfSxcclxuICAgIH0sXHJcbiBcclxuICAgIC8vIHVzZSB0aGlzIGZvciBpbml0aWFsaXphdGlvblxyXG4gICAgb25Mb2FkOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gZmFsc2U7XHJcbiAgICAgICAgLy8g5Yqg6YCf5bqm5pa55ZCR5byA5YWzXHJcbiAgICAgICAgdGhpcy5hY2NMZWZ0ID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5hY2NSaWdodCA9IGZhbHNlO1xyXG4gICAgICAgIC8vIOS4u+inkuW9k+WJjeawtOW5s+aWueWQkemAn+W6plxyXG4gICAgICAgIHRoaXMueFNwZWVkID0gMDtcclxuICAgICAgICAvLyBzY3JlZW4gYm91bmRhcmllc1xyXG4gICAgICAgIHRoaXMubWluUG9zWCA9IC10aGlzLm5vZGUucGFyZW50LndpZHRoLzI7XHJcbiAgICAgICAgdGhpcy5tYXhQb3NYID0gdGhpcy5ub2RlLnBhcmVudC53aWR0aC8yO1xyXG4gXHJcbiAgICAgICAgLy8g5Yid5aeL5YyW6ZSu55uY6L6T5YWl55uR5ZCsXHJcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQub24oY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9ET1dOLCB0aGlzLm9uS2V5RG93biwgdGhpcyk7XHJcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQub24oY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9VUCwgdGhpcy5vbktleVVwLCB0aGlzKTtcclxuICAgICAgICBcclxuICAgICAgICB2YXIgdG91Y2hSZWNlaXZlciA9IGNjLkNhbnZhcy5pbnN0YW5jZS5ub2RlO1xyXG4gICAgICAgIHRvdWNoUmVjZWl2ZXIub24oJ3RvdWNoc3RhcnQnLCB0aGlzLm9uVG91Y2hTdGFydCwgdGhpcyk7XHJcbiAgICAgICAgdG91Y2hSZWNlaXZlci5vbigndG91Y2hlbmQnLCB0aGlzLm9uVG91Y2hFbmQsIHRoaXMpO1xyXG4gICAgfSxcclxuIFxyXG4gICAgb25EZXN0cm95ICgpIHtcclxuICAgICAgICAvLyDlj5bmtojplK7nm5jovpPlhaXnm5HlkKxcclxuICAgICAgICBjYy5zeXN0ZW1FdmVudC5vZmYoY2MuU3lzdGVtRXZlbnQuRXZlbnRUeXBlLktFWV9ET1dOLCB0aGlzLm9uS2V5RG93biwgdGhpcyk7XHJcbiAgICAgICAgY2Muc3lzdGVtRXZlbnQub2ZmKGNjLlN5c3RlbUV2ZW50LkV2ZW50VHlwZS5LRVlfVVAsIHRoaXMub25LZXlVcCwgdGhpcyk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdmFyIHRvdWNoUmVjZWl2ZXIgPSBjYy5DYW52YXMuaW5zdGFuY2Uubm9kZTtcclxuICAgICAgICB0b3VjaFJlY2VpdmVyLm9mZigndG91Y2hzdGFydCcsIHRoaXMub25Ub3VjaFN0YXJ0LCB0aGlzKTtcclxuICAgICAgICB0b3VjaFJlY2VpdmVyLm9mZigndG91Y2hlbmQnLCB0aGlzLm9uVG91Y2hFbmQsIHRoaXMpO1xyXG4gICAgfSxcclxuIFxyXG4gICAgb25LZXlEb3duIChldmVudCkge1xyXG4gICAgICAgIHN3aXRjaChldmVudC5rZXlDb2RlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLmE6XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLmxlZnQ6XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjY0xlZnQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hY2NSaWdodCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLmQ6XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLnJpZ2h0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5hY2NMZWZ0ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjY1JpZ2h0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiBcclxuICAgIG9uS2V5VXAgKGV2ZW50KSB7XHJcbiAgICAgICAgc3dpdGNoKGV2ZW50LmtleUNvZGUpIHtcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkuYTpcclxuICAgICAgICAgICAgY2FzZSBjYy5tYWNyby5LRVkubGVmdDpcclxuICAgICAgICAgICAgICAgIHRoaXMuYWNjTGVmdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLmQ6XHJcbiAgICAgICAgICAgIGNhc2UgY2MubWFjcm8uS0VZLnJpZ2h0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5hY2NSaWdodCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuIFxyXG4gICAgb25Ub3VjaFN0YXJ0IChldmVudCkge1xyXG4gICAgICAgIHZhciB0b3VjaExvYyA9IGV2ZW50LmdldExvY2F0aW9uKCk7XHJcbiAgICAgICAgaWYgKHRvdWNoTG9jLnggPj0gY2Mud2luU2l6ZS53aWR0aC8yKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWNjTGVmdCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmFjY1JpZ2h0ID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmFjY0xlZnQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmFjY1JpZ2h0ID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuIFxyXG4gICAgb25Ub3VjaEVuZCAoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmFjY0xlZnQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmFjY1JpZ2h0ID0gZmFsc2U7XHJcbiAgICB9LFxyXG4gXHJcbiAgICBydW5KdW1wQWN0aW9uOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgLy8g6Lez6LeD5LiK5Y2HXHJcbiAgICAgICAgdmFyIGp1bXBVcCA9IGNjLnR3ZWVuKCkuYnkodGhpcy5qdW1wRHVyYXRpb24sIHsgeTogdGhpcy5qdW1wSGVpZ2h0IH0sIHsgZWFzaW5nOiAnc2luZU91dCcgfSk7XHJcbiAgICAgICAgLy8g5LiL6JC9XHJcbiAgICAgICAgdmFyIGp1bXBEb3duID0gY2MudHdlZW4oKS5ieSh0aGlzLmp1bXBEdXJhdGlvbiwgeyB5OiAtdGhpcy5qdW1wSGVpZ2h0IH0sIHsgZWFzaW5nOiAnc2luZUluJyB9KTtcclxuICAgICAgICAvLyDlvaLlj5hcclxuICAgICAgICB2YXIgc3F1YXNoID0gY2MudHdlZW4oKS50byh0aGlzLnNxdWFzaER1cmF0aW9uLCB7IHNjYWxlWDogMSwgc2NhbGVZOiAwLjYgfSk7XHJcbiAgICAgICAgdmFyIHN0cmV0Y2ggPSBjYy50d2VlbigpLnRvKHRoaXMuc3F1YXNoRHVyYXRpb24sIHsgc2NhbGVYOiAxLCBzY2FsZVk6IDEuMiB9KTtcclxuICAgICAgICB2YXIgc2NhbGVCYWNrID0gY2MudHdlZW4oKS50byh0aGlzLnNxdWFzaER1cmF0aW9uLCB7IHNjYWxlWDogMSwgc2NhbGVZOiAxIH0pO1xyXG4gXHJcbiAgICAgICAgLy8g5Yib5bu65LiA5Liq57yT5YqoXHJcbiAgICAgICAgdmFyIHR3ZWVuID0gY2MudHdlZW4oKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyDmjInpobrluo/miafooYzliqjkvZxcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnNlcXVlbmNlKHNxdWFzaCwgc3RyZXRjaCwganVtcFVwLCBzY2FsZUJhY2ssIGp1bXBEb3duKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyDmt7vliqDkuIDkuKrlm57osIPlh73mlbDvvIzlnKjliY3pnaLnmoTliqjkvZzpg73nu5PmnZ/ml7bosIPnlKjmiJHku6zlrprkuYnnmoQgcGxheUp1bXBTb3VuZCgpIOaWueazlVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2FsbCh0aGlzLnBsYXlKdW1wU291bmQsIHRoaXMpO1xyXG4gXHJcbiAgICAgICAgLy8g5LiN5pat6YeN5aSNXHJcbiAgICAgICAgcmV0dXJuIGNjLnR3ZWVuKCkucmVwZWF0Rm9yZXZlcih0d2Vlbik7XHJcbiAgICB9LFxyXG4gXHJcbiAgICBwbGF5SnVtcFNvdW5kOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgLy8g6LCD55So5aOw6Z+z5byV5pOO5pKt5pS+5aOw6Z+zXHJcbiAgICAgICAgY2MuYXVkaW9FbmdpbmUucGxheUVmZmVjdCh0aGlzLmp1bXBBdWRpbywgZmFsc2UpO1xyXG4gICAgfSxcclxuIFxyXG4gICAgZ2V0Q2VudGVyUG9zOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGNlbnRlclBvcyA9IGNjLnYyKHRoaXMubm9kZS54LCB0aGlzLm5vZGUueSArIHRoaXMubm9kZS5oZWlnaHQvMik7XHJcbiAgICAgICAgcmV0dXJuIGNlbnRlclBvcztcclxuICAgIH0sXHJcbiBcclxuICAgIHN0YXJ0TW92ZUF0OiBmdW5jdGlvbiAocG9zKSB7XHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnhTcGVlZCA9IDA7XHJcbiAgICAgICAgdGhpcy5ub2RlLnNldFBvc2l0aW9uKHBvcyk7XHJcbiBcclxuICAgICAgICB2YXIganVtcEFjdGlvbiA9IHRoaXMucnVuSnVtcEFjdGlvbigpO1xyXG4gICAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSkudGhlbihqdW1wQWN0aW9uKS5zdGFydCgpXHJcbiAgICB9LFxyXG4gXHJcbiAgICBzdG9wTW92ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMubm9kZS5zdG9wQWxsQWN0aW9ucygpO1xyXG4gICAgfSxcclxuIFxyXG4gICAgLy8gY2FsbGVkIGV2ZXJ5IGZyYW1lXHJcbiAgICB1cGRhdGU6IGZ1bmN0aW9uIChkdCkge1xyXG4gICAgICAgIC8vIOagueaNruW9k+WJjeWKoOmAn+W6puaWueWQkeavj+W4p+abtOaWsOmAn+W6plxyXG4gICAgICAgIGlmICh0aGlzLmFjY0xlZnQpIHtcclxuICAgICAgICAgICAgdGhpcy54U3BlZWQgLT0gdGhpcy5hY2NlbCAqIGR0O1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5hY2NSaWdodCkge1xyXG4gICAgICAgICAgICB0aGlzLnhTcGVlZCArPSB0aGlzLmFjY2VsICogZHQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIOmZkOWItuS4u+inkueahOmAn+W6puS4jeiDvei2hei/h+acgOWkp+WAvFxyXG4gICAgICAgIGlmICggTWF0aC5hYnModGhpcy54U3BlZWQpID4gdGhpcy5tYXhNb3ZlU3BlZWQgKSB7XHJcbiAgICAgICAgICAgIC8vIGlmIHNwZWVkIHJlYWNoIGxpbWl0LCB1c2UgbWF4IHNwZWVkIHdpdGggY3VycmVudCBkaXJlY3Rpb25cclxuICAgICAgICAgICAgdGhpcy54U3BlZWQgPSB0aGlzLm1heE1vdmVTcGVlZCAqIHRoaXMueFNwZWVkIC8gTWF0aC5hYnModGhpcy54U3BlZWQpO1xyXG4gICAgICAgIH1cclxuIFxyXG4gICAgICAgIC8vIOagueaNruW9k+WJjemAn+W6puabtOaWsOS4u+inkueahOS9jee9rlxyXG4gICAgICAgIHRoaXMubm9kZS54ICs9IHRoaXMueFNwZWVkICogZHQ7XHJcbiBcclxuICAgICAgICAvLyBsaW1pdCBwbGF5ZXIgcG9zaXRpb24gaW5zaWRlIHNjcmVlblxyXG4gICAgICAgIGlmICggdGhpcy5ub2RlLnggPiB0aGlzLm5vZGUucGFyZW50LndpZHRoLzIpIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLnggPSB0aGlzLm5vZGUucGFyZW50LndpZHRoLzI7XHJcbiAgICAgICAgICAgIHRoaXMueFNwZWVkID0gMDtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMubm9kZS54IDwgLXRoaXMubm9kZS5wYXJlbnQud2lkdGgvMikge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUueCA9IC10aGlzLm5vZGUucGFyZW50LndpZHRoLzI7XHJcbiAgICAgICAgICAgIHRoaXMueFNwZWVkID0gMDtcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG59KTsiXX0=
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Star.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b172bu65n9NXaXmZQ2uIIVh', 'Star');
// scripts/Star.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    // 星星和主角之间的距离小于这个数值时，就会完成收集
    pickRadius: 0
  },
  getPlayerDistance: function getPlayerDistance() {
    // 根据Player节点位置判断距离
    var playerPos = this.game.player.getCenterPos(); // 根据两点位置计算两点之间距离

    var dist = this.node.position.sub(playerPos).mag();
    return dist;
  },
  onPicked: function onPicked() {
    var pos = this.node.getPosition(); // 调用 Game 脚本的得分方法

    this.game.gainScore(pos); // 当星星被收集时，调用 Game 脚本中的接口，销毁当前星星节点，生成一个新的星星

    this.game.despawnStar(this.node);
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    this.enabled = false;
  },
  // use this for initialization
  init: function init(game) {
    this.game = game;
    this.enabled = true;
    this.node.opacity = 255;
  },
  reuse: function reuse(game) {
    this.init(game);
  },
  start: function start() {},
  update: function update(dt) {
    // 每帧判断星星和主角之间的距离是否小于收集距离
    if (this.getPlayerDistance() < this.pickRadius) {
      // 调用收集行为
      this.onPicked();
      return;
    } // 根据Game脚本中的计时器更新星星的透明度


    var opacityRatio = 1 - this.game.timer / this.game.starDuration;
    var minOpcity = 50;
    this.node.opacity = minOpcity + Math.floor(opacityRatio * (255 - minOpcity));
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU3Rhci5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsInBpY2tSYWRpdXMiLCJnZXRQbGF5ZXJEaXN0YW5jZSIsInBsYXllclBvcyIsImdhbWUiLCJwbGF5ZXIiLCJnZXRDZW50ZXJQb3MiLCJkaXN0Iiwibm9kZSIsInBvc2l0aW9uIiwic3ViIiwibWFnIiwib25QaWNrZWQiLCJwb3MiLCJnZXRQb3NpdGlvbiIsImdhaW5TY29yZSIsImRlc3Bhd25TdGFyIiwib25Mb2FkIiwiZW5hYmxlZCIsImluaXQiLCJvcGFjaXR5IiwicmV1c2UiLCJzdGFydCIsInVwZGF0ZSIsImR0Iiwib3BhY2l0eVJhdGlvIiwidGltZXIiLCJzdGFyRHVyYXRpb24iLCJtaW5PcGNpdHkiLCJNYXRoIiwiZmxvb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSO0FBQ0FDLElBQUFBLFVBQVUsRUFBRTtBQUZKLEdBSFA7QUFPTEMsRUFBQUEsaUJBUEssK0JBT2U7QUFDaEI7QUFDQSxRQUFJQyxTQUFTLEdBQUcsS0FBS0MsSUFBTCxDQUFVQyxNQUFWLENBQWlCQyxZQUFqQixFQUFoQixDQUZnQixDQUdoQjs7QUFDQSxRQUFJQyxJQUFJLEdBQUcsS0FBS0MsSUFBTCxDQUFVQyxRQUFWLENBQW1CQyxHQUFuQixDQUF1QlAsU0FBdkIsRUFBa0NRLEdBQWxDLEVBQVg7QUFDQSxXQUFPSixJQUFQO0FBQ0gsR0FiSTtBQWNMSyxFQUFBQSxRQWRLLHNCQWNNO0FBQ1AsUUFBSUMsR0FBRyxHQUFHLEtBQUtMLElBQUwsQ0FBVU0sV0FBVixFQUFWLENBRE8sQ0FFUDs7QUFDQSxTQUFLVixJQUFMLENBQVVXLFNBQVYsQ0FBb0JGLEdBQXBCLEVBSE8sQ0FJUDs7QUFDQSxTQUFLVCxJQUFMLENBQVVZLFdBQVYsQ0FBc0IsS0FBS1IsSUFBM0I7QUFDSCxHQXBCSTtBQXFCTDtBQUVBUyxFQUFBQSxNQXZCSyxvQkF1Qks7QUFDTixTQUFLQyxPQUFMLEdBQWUsS0FBZjtBQUNILEdBekJJO0FBMEJMO0FBQ0FDLEVBQUFBLElBM0JLLGdCQTJCQ2YsSUEzQkQsRUEyQk87QUFDUixTQUFLQSxJQUFMLEdBQVlBLElBQVo7QUFDQSxTQUFLYyxPQUFMLEdBQWUsSUFBZjtBQUNBLFNBQUtWLElBQUwsQ0FBVVksT0FBVixHQUFvQixHQUFwQjtBQUNILEdBL0JJO0FBaUNMQyxFQUFBQSxLQWpDSyxpQkFpQ0VqQixJQWpDRixFQWlDUTtBQUNULFNBQUtlLElBQUwsQ0FBVWYsSUFBVjtBQUNILEdBbkNJO0FBb0NMa0IsRUFBQUEsS0FwQ0ssbUJBb0NJLENBRVIsQ0F0Q0k7QUF3Q0xDLEVBQUFBLE1BeENLLGtCQXdDR0MsRUF4Q0gsRUF3Q087QUFDUjtBQUNBLFFBQUksS0FBS3RCLGlCQUFMLEtBQTJCLEtBQUtELFVBQXBDLEVBQWdEO0FBQzVDO0FBQ0EsV0FBS1csUUFBTDtBQUNBO0FBQ0gsS0FOTyxDQU9SOzs7QUFDQSxRQUFJYSxZQUFZLEdBQUcsSUFBSSxLQUFLckIsSUFBTCxDQUFVc0IsS0FBVixHQUFnQixLQUFLdEIsSUFBTCxDQUFVdUIsWUFBakQ7QUFDQSxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQSxTQUFLcEIsSUFBTCxDQUFVWSxPQUFWLEdBQW9CUSxTQUFTLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXTCxZQUFZLElBQUksTUFBTUcsU0FBVixDQUF2QixDQUFoQztBQUNIO0FBbkRJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9jbGFzcy5odG1sXHJcbi8vIExlYXJuIEF0dHJpYnV0ZTpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICAvLyDmmJ/mmJ/lkozkuLvop5LkuYvpl7TnmoTot53nprvlsI/kuo7ov5nkuKrmlbDlgLzml7bvvIzlsLHkvJrlrozmiJDmlLbpm4ZcclxuICAgICAgICBwaWNrUmFkaXVzOiAwLFxyXG4gICAgfSxcclxuICAgIGdldFBsYXllckRpc3RhbmNlKCkge1xyXG4gICAgICAgIC8vIOagueaNrlBsYXllcuiKgueCueS9jee9ruWIpOaWrei3neemu1xyXG4gICAgICAgIHZhciBwbGF5ZXJQb3MgPSB0aGlzLmdhbWUucGxheWVyLmdldENlbnRlclBvcygpO1xyXG4gICAgICAgIC8vIOagueaNruS4pOeCueS9jee9ruiuoeeul+S4pOeCueS5i+mXtOi3neemu1xyXG4gICAgICAgIHZhciBkaXN0ID0gdGhpcy5ub2RlLnBvc2l0aW9uLnN1YihwbGF5ZXJQb3MpLm1hZygpO1xyXG4gICAgICAgIHJldHVybiBkaXN0O1xyXG4gICAgfSxcclxuICAgIG9uUGlja2VkKCkge1xyXG4gICAgICAgIHZhciBwb3MgPSB0aGlzLm5vZGUuZ2V0UG9zaXRpb24oKTtcclxuICAgICAgICAvLyDosIPnlKggR2FtZSDohJrmnKznmoTlvpfliIbmlrnms5VcclxuICAgICAgICB0aGlzLmdhbWUuZ2FpblNjb3JlKHBvcyk7XHJcbiAgICAgICAgLy8g5b2T5pif5pif6KKr5pS26ZuG5pe277yM6LCD55SoIEdhbWUg6ISa5pys5Lit55qE5o6l5Y+j77yM6ZSA5q+B5b2T5YmN5pif5pif6IqC54K577yM55Sf5oiQ5LiA5Liq5paw55qE5pif5pifXHJcbiAgICAgICAgdGhpcy5nYW1lLmRlc3Bhd25TdGFyKHRoaXMubm9kZSk7XHJcbiAgICB9LFxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuICAgICAgICB0aGlzLmVuYWJsZWQgPSBmYWxzZTtcclxuICAgIH0sXHJcbiAgICAvLyB1c2UgdGhpcyBmb3IgaW5pdGlhbGl6YXRpb25cclxuICAgIGluaXQgKGdhbWUpIHtcclxuICAgICAgICB0aGlzLmdhbWUgPSBnYW1lO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICB9LFxyXG5cclxuICAgIHJldXNlIChnYW1lKSB7XHJcbiAgICAgICAgdGhpcy5pbml0KGdhbWUpO1xyXG4gICAgfSxcclxuICAgIHN0YXJ0ICgpIHtcclxuXHJcbiAgICB9LFxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICAvLyDmr4/luKfliKTmlq3mmJ/mmJ/lkozkuLvop5LkuYvpl7TnmoTot53nprvmmK/lkKblsI/kuo7mlLbpm4bot53nprtcclxuICAgICAgICBpZiAodGhpcy5nZXRQbGF5ZXJEaXN0YW5jZSgpIDwgdGhpcy5waWNrUmFkaXVzKSB7XHJcbiAgICAgICAgICAgIC8vIOiwg+eUqOaUtumbhuihjOS4ulxyXG4gICAgICAgICAgICB0aGlzLm9uUGlja2VkKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8g5qC55o2uR2FtZeiEmuacrOS4reeahOiuoeaXtuWZqOabtOaWsOaYn+aYn+eahOmAj+aYjuW6plxyXG4gICAgICAgIHZhciBvcGFjaXR5UmF0aW8gPSAxIC0gdGhpcy5nYW1lLnRpbWVyL3RoaXMuZ2FtZS5zdGFyRHVyYXRpb247XHJcbiAgICAgICAgdmFyIG1pbk9wY2l0eSA9IDUwO1xyXG4gICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gbWluT3BjaXR5ICsgTWF0aC5mbG9vcihvcGFjaXR5UmF0aW8gKiAoMjU1IC0gbWluT3BjaXR5KSk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2050d5nfIdKuZ7JojDiRfk2', 'Game');
// scripts/Game.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var Player = require('Player');

var ScoreFX = require('ScoreFX');

var Star = require('Star');

var Game = cc.Class({
  "extends": cc.Component,
  properties: {
    // 这个属性引用了星星预置资源
    starPrefab: {
      "default": null,
      type: cc.Prefab
    },
    scoreFXPrefab: {
      "default": null,
      type: cc.Prefab
    },
    // 星星产生后消失时间的随机范围
    maxStarDuration: 0,
    minStarDuration: 0,
    // 地面节点，用于确定星星生成的高度
    ground: {
      "default": null,
      type: cc.Node
    },
    // Player节点，用于获取主角弹跳的高度和控制主角行动开关
    player: {
      "default": null,
      type: Player
    },
    // score label 的引用
    scoreDisplay: {
      "default": null,
      type: cc.Label
    },
    // 得分音效资源
    scoreAudio: {
      "default": null,
      type: cc.AudioClip
    },
    btnNode: {
      "default": null,
      type: cc.Node
    },
    gameOverNode: {
      "default": null,
      type: cc.Node
    },
    controlHintLabel: {
      "default": null,
      type: cc.Label
    },
    keyboardHint: {
      "default": '',
      multiline: true
    },
    touchHint: {
      "default": '',
      multiline: true
    }
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    // 获取地平面的y轴坐标
    this.groundY = this.ground.y + this.ground.height / 2; // store last star's x position

    this.currentStar = null;
    this.currentStarX = 0; // 初始化计时器

    this.timer = 0;
    this.starDuration = 0; // is showing menu or running game

    this.enabled = false; // initialize control hint

    var hintText = cc.sys.isMobile ? this.touchHint : this.keyboardHint;
    this.controlHintLabel.string = hintText; // initialize star and score pool

    this.starPool = new cc.NodePool('Star');
    this.scorePool = new cc.NodePool('ScoreFX');
  },
  onStartGame: function onStartGame() {
    // 初始化计分
    this.resetScore(); // set game state to running

    this.enabled = true; // set button and gameover text out of screen

    this.btnNode.x = 3000;
    this.gameOverNode.active = false; // reset player position and move speed

    this.player.startMoveAt(cc.v2(0, this.groundY)); // spawn star

    this.spawnNewStar();
  },
  spawnNewStar: function spawnNewStar() {
    var newStar = null; // 使用给定的模板在场景中生成一个新节点

    if (this.starPool.size() > 0) {
      newStar = this.starPool.get(this); // this will be passed to Star's reuse method
    } else {
      newStar = cc.instantiate(this.starPrefab);
    } // 将新增的节点添加到Canvas节点下面


    this.node.addChild(newStar); // 为星星设置一个随机位置

    newStar.setPosition(this.getNewStarPosition()); // 在星星脚本组件上保存Game对象的引用

    newStar.getComponent('Star').init(this); // start star timer and store star reference

    this.startTimer();
    this.currentStar = newStar;
  },
  despawnStar: function despawnStar(star) {
    this.starPool.put(star);
    this.spawnNewStar();
  },
  startTimer: function startTimer() {
    // get a life duration for next star
    this.starDuration = this.minStarDuration + Math.random() * (this.maxStarDuration - this.minStarDuration);
    this.timer = 0;
  },
  getNewStarPosition: function getNewStarPosition() {
    // if there's no star, set a random x pos
    if (!this.currentStar) {
      this.currentStarX = (Math.random() - 0.5) * 2 * this.node.width / 2;
    }

    var randX = 0; // 根据地平面位置和主角跳跃高度，随机得到一个星星的y坐标

    var randY = this.groundY + Math.random() * this.player.getComponent('Player').jumpHeight + 50; // 根据屏幕宽度，随机得到一个星星x坐标

    var maxX = this.node.width / 2;

    if (this.currentStarX >= 0) {
      randX = -Math.random() * maxX;
    } else {
      randX = Math.random() * maxX;
    }

    this.currentStarX = randX; // 返回星星坐标

    return cc.v2(randX, randY);
  },
  gainScore: function gainScore(pos) {
    this.score += 1; // 更新 scoreDisplay Label 的文字

    this.scoreDisplay.string = 'Score: ' + this.score.toString(); // 播放特效

    var fx = this.spawnScoreFX();
    this.node.addChild(fx.node);
    fx.node.setPosition(pos);
    fx.play(); // 播放得分音效

    cc.audioEngine.playEffect(this.scoreAudio, false);
  },
  update: function update(dt) {
    // 每帧更新计时器，超过限度还没有生成新的星星
    // 就会调用游戏失败逻辑
    if (this.timer > this.starDuration) {
      this.gameOver();
      this.enabled = false; // disable this to avoid gameOver() repeatedly

      return;
    }

    this.timer += dt;
  },
  resetScore: function resetScore() {
    this.score = 0;
    this.scoreDisplay.string = 'Score: ' + this.score.toString();
  },
  spawnScoreFX: function spawnScoreFX() {
    var fx;

    if (this.scorePool.size() > 0) {
      fx = this.scorePool.get();
      return fx.getComponent('ScoreFX');
    } else {
      fx = cc.instantiate(this.scoreFXPrefab).getComponent('ScoreFX');
      fx.init(this);
      return fx;
    }
  },
  despawnScoreFX: function despawnScoreFX(scoreFX) {
    this.scorePool.put(scoreFX);
  },
  gameOver: function gameOver() {
    this.gameOverNode.active = true;
    this.player.enabled = false;
    this.player.stopMove();
    this.currentStar.destroy();
    this.btnNode.x = 0;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcR2FtZS5qcyJdLCJuYW1lcyI6WyJQbGF5ZXIiLCJyZXF1aXJlIiwiU2NvcmVGWCIsIlN0YXIiLCJHYW1lIiwiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJzdGFyUHJlZmFiIiwidHlwZSIsIlByZWZhYiIsInNjb3JlRlhQcmVmYWIiLCJtYXhTdGFyRHVyYXRpb24iLCJtaW5TdGFyRHVyYXRpb24iLCJncm91bmQiLCJOb2RlIiwicGxheWVyIiwic2NvcmVEaXNwbGF5IiwiTGFiZWwiLCJzY29yZUF1ZGlvIiwiQXVkaW9DbGlwIiwiYnRuTm9kZSIsImdhbWVPdmVyTm9kZSIsImNvbnRyb2xIaW50TGFiZWwiLCJrZXlib2FyZEhpbnQiLCJtdWx0aWxpbmUiLCJ0b3VjaEhpbnQiLCJvbkxvYWQiLCJncm91bmRZIiwieSIsImhlaWdodCIsImN1cnJlbnRTdGFyIiwiY3VycmVudFN0YXJYIiwidGltZXIiLCJzdGFyRHVyYXRpb24iLCJlbmFibGVkIiwiaGludFRleHQiLCJzeXMiLCJpc01vYmlsZSIsInN0cmluZyIsInN0YXJQb29sIiwiTm9kZVBvb2wiLCJzY29yZVBvb2wiLCJvblN0YXJ0R2FtZSIsInJlc2V0U2NvcmUiLCJ4IiwiYWN0aXZlIiwic3RhcnRNb3ZlQXQiLCJ2MiIsInNwYXduTmV3U3RhciIsIm5ld1N0YXIiLCJzaXplIiwiZ2V0IiwiaW5zdGFudGlhdGUiLCJub2RlIiwiYWRkQ2hpbGQiLCJzZXRQb3NpdGlvbiIsImdldE5ld1N0YXJQb3NpdGlvbiIsImdldENvbXBvbmVudCIsImluaXQiLCJzdGFydFRpbWVyIiwiZGVzcGF3blN0YXIiLCJzdGFyIiwicHV0IiwiTWF0aCIsInJhbmRvbSIsIndpZHRoIiwicmFuZFgiLCJyYW5kWSIsImp1bXBIZWlnaHQiLCJtYXhYIiwiZ2FpblNjb3JlIiwicG9zIiwic2NvcmUiLCJ0b1N0cmluZyIsImZ4Iiwic3Bhd25TY29yZUZYIiwicGxheSIsImF1ZGlvRW5naW5lIiwicGxheUVmZmVjdCIsInVwZGF0ZSIsImR0IiwiZ2FtZU92ZXIiLCJkZXNwYXduU2NvcmVGWCIsInNjb3JlRlgiLCJzdG9wTW92ZSIsImRlc3Ryb3kiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBTUEsTUFBTSxHQUFHQyxPQUFPLENBQUMsUUFBRCxDQUF0Qjs7QUFDQSxJQUFNQyxPQUFPLEdBQUdELE9BQU8sQ0FBQyxTQUFELENBQXZCOztBQUNBLElBQU1FLElBQUksR0FBR0YsT0FBTyxDQUFDLE1BQUQsQ0FBcEI7O0FBQ0EsSUFBSUcsSUFBSSxHQUFHQyxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNoQixhQUFTRCxFQUFFLENBQUNFLFNBREk7QUFHaEJDLEVBQUFBLFVBQVUsRUFBRTtBQUNWO0FBQ0FDLElBQUFBLFVBQVUsRUFBRTtBQUNSLGlCQUFTLElBREQ7QUFFUkMsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNNO0FBRkQsS0FGRjtBQU1WQyxJQUFBQSxhQUFhLEVBQUU7QUFDYixpQkFBUyxJQURJO0FBRWJGLE1BQUFBLElBQUksRUFBRUwsRUFBRSxDQUFDTTtBQUZJLEtBTkw7QUFVVjtBQUNBRSxJQUFBQSxlQUFlLEVBQUUsQ0FYUDtBQVlWQyxJQUFBQSxlQUFlLEVBQUUsQ0FaUDtBQWNWO0FBQ0FDLElBQUFBLE1BQU0sRUFBRTtBQUNKLGlCQUFTLElBREw7QUFFSkwsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNXO0FBRkwsS0FmRTtBQW9CVjtBQUNBQyxJQUFBQSxNQUFNLEVBQUU7QUFDSixpQkFBUyxJQURMO0FBRUpQLE1BQUFBLElBQUksRUFBRVY7QUFGRixLQXJCRTtBQTBCVjtBQUNBa0IsSUFBQUEsWUFBWSxFQUFFO0FBQ1YsaUJBQVMsSUFEQztBQUVWUixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ2M7QUFGQyxLQTNCSjtBQStCVjtBQUNBQyxJQUFBQSxVQUFVLEVBQUU7QUFDUixpQkFBUyxJQUREO0FBRVJWLE1BQUFBLElBQUksRUFBRUwsRUFBRSxDQUFDZ0I7QUFGRCxLQWhDRjtBQW9DVkMsSUFBQUEsT0FBTyxFQUFFO0FBQ1AsaUJBQVMsSUFERjtBQUVQWixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1c7QUFGRixLQXBDQztBQXdDVk8sSUFBQUEsWUFBWSxFQUFFO0FBQ1osaUJBQVMsSUFERztBQUVaYixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1c7QUFGRyxLQXhDSjtBQTRDVlEsSUFBQUEsZ0JBQWdCLEVBQUU7QUFDaEIsaUJBQVMsSUFETztBQUVoQmQsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNjO0FBRk8sS0E1Q1I7QUFnRFZNLElBQUFBLFlBQVksRUFBRTtBQUNaLGlCQUFTLEVBREc7QUFFWkMsTUFBQUEsU0FBUyxFQUFFO0FBRkMsS0FoREo7QUFvRFZDLElBQUFBLFNBQVMsRUFBRTtBQUNULGlCQUFTLEVBREE7QUFFVEQsTUFBQUEsU0FBUyxFQUFFO0FBRkY7QUFwREQsR0FISTtBQTZEaEI7QUFFQUUsRUFBQUEsTUEvRGdCLG9CQStETjtBQUNOO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEtBQUtkLE1BQUwsQ0FBWWUsQ0FBWixHQUFnQixLQUFLZixNQUFMLENBQVlnQixNQUFaLEdBQW1CLENBQWxELENBRk0sQ0FHTjs7QUFDQSxTQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixDQUFwQixDQUxNLENBT047O0FBQ0EsU0FBS0MsS0FBTCxHQUFhLENBQWI7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLENBQXBCLENBVE0sQ0FXTjs7QUFDQSxTQUFLQyxPQUFMLEdBQWUsS0FBZixDQVpNLENBYU47O0FBQ0EsUUFBSUMsUUFBUSxHQUFHaEMsRUFBRSxDQUFDaUMsR0FBSCxDQUFPQyxRQUFQLEdBQWtCLEtBQUtaLFNBQXZCLEdBQW1DLEtBQUtGLFlBQXZEO0FBQ0EsU0FBS0QsZ0JBQUwsQ0FBc0JnQixNQUF0QixHQUErQkgsUUFBL0IsQ0FmTSxDQWlCTjs7QUFDQSxTQUFLSSxRQUFMLEdBQWdCLElBQUlwQyxFQUFFLENBQUNxQyxRQUFQLENBQWdCLE1BQWhCLENBQWhCO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQixJQUFJdEMsRUFBRSxDQUFDcUMsUUFBUCxDQUFnQixTQUFoQixDQUFqQjtBQUlILEdBdEZlO0FBdUZoQkUsRUFBQUEsV0F2RmdCLHlCQXVGRDtBQUNYO0FBQ0EsU0FBS0MsVUFBTCxHQUZXLENBR1g7O0FBQ0EsU0FBS1QsT0FBTCxHQUFlLElBQWYsQ0FKVyxDQUtYOztBQUNBLFNBQUtkLE9BQUwsQ0FBYXdCLENBQWIsR0FBaUIsSUFBakI7QUFDQSxTQUFLdkIsWUFBTCxDQUFrQndCLE1BQWxCLEdBQTJCLEtBQTNCLENBUFcsQ0FRWDs7QUFDQSxTQUFLOUIsTUFBTCxDQUFZK0IsV0FBWixDQUF3QjNDLEVBQUUsQ0FBQzRDLEVBQUgsQ0FBTSxDQUFOLEVBQVMsS0FBS3BCLE9BQWQsQ0FBeEIsRUFUVyxDQVVYOztBQUNBLFNBQUtxQixZQUFMO0FBQ0gsR0FuR2U7QUFvR2hCQSxFQUFBQSxZQXBHZ0IsMEJBb0dEO0FBQ1gsUUFBSUMsT0FBTyxHQUFHLElBQWQsQ0FEVyxDQUVYOztBQUNBLFFBQUksS0FBS1YsUUFBTCxDQUFjVyxJQUFkLEtBQXVCLENBQTNCLEVBQThCO0FBQzFCRCxNQUFBQSxPQUFPLEdBQUcsS0FBS1YsUUFBTCxDQUFjWSxHQUFkLENBQWtCLElBQWxCLENBQVYsQ0FEMEIsQ0FDUztBQUN0QyxLQUZELE1BRU87QUFDSEYsTUFBQUEsT0FBTyxHQUFHOUMsRUFBRSxDQUFDaUQsV0FBSCxDQUFlLEtBQUs3QyxVQUFwQixDQUFWO0FBQ0gsS0FQVSxDQVFYOzs7QUFDQSxTQUFLOEMsSUFBTCxDQUFVQyxRQUFWLENBQW1CTCxPQUFuQixFQVRXLENBVVg7O0FBQ0FBLElBQUFBLE9BQU8sQ0FBQ00sV0FBUixDQUFvQixLQUFLQyxrQkFBTCxFQUFwQixFQVhXLENBWVg7O0FBQ0FQLElBQUFBLE9BQU8sQ0FBQ1EsWUFBUixDQUFxQixNQUFyQixFQUE2QkMsSUFBN0IsQ0FBa0MsSUFBbEMsRUFiVyxDQWNYOztBQUNBLFNBQUtDLFVBQUw7QUFDQSxTQUFLN0IsV0FBTCxHQUFtQm1CLE9BQW5CO0FBQ0gsR0FySGU7QUFzSGhCVyxFQUFBQSxXQXRIZ0IsdUJBc0hIQyxJQXRIRyxFQXNIRztBQUNmLFNBQUt0QixRQUFMLENBQWN1QixHQUFkLENBQWtCRCxJQUFsQjtBQUNBLFNBQUtiLFlBQUw7QUFDSCxHQXpIZTtBQTBIaEJXLEVBQUFBLFVBMUhnQix3QkEwSEY7QUFDVjtBQUNBLFNBQUsxQixZQUFMLEdBQW9CLEtBQUtyQixlQUFMLEdBQXVCbUQsSUFBSSxDQUFDQyxNQUFMLE1BQWlCLEtBQUtyRCxlQUFMLEdBQXVCLEtBQUtDLGVBQTdDLENBQTNDO0FBQ0EsU0FBS29CLEtBQUwsR0FBYSxDQUFiO0FBQ0gsR0E5SGU7QUErSGhCd0IsRUFBQUEsa0JBL0hnQixnQ0ErSEs7QUFDakI7QUFDQSxRQUFJLENBQUMsS0FBSzFCLFdBQVYsRUFBdUI7QUFDbkIsV0FBS0MsWUFBTCxHQUFvQixDQUFDZ0MsSUFBSSxDQUFDQyxNQUFMLEtBQWdCLEdBQWpCLElBQXdCLENBQXhCLEdBQTRCLEtBQUtYLElBQUwsQ0FBVVksS0FBdEMsR0FBNEMsQ0FBaEU7QUFDSDs7QUFDRCxRQUFJQyxLQUFLLEdBQUcsQ0FBWixDQUxpQixDQU1qQjs7QUFDQSxRQUFJQyxLQUFLLEdBQUcsS0FBS3hDLE9BQUwsR0FBZW9DLElBQUksQ0FBQ0MsTUFBTCxLQUFnQixLQUFLakQsTUFBTCxDQUFZMEMsWUFBWixDQUF5QixRQUF6QixFQUFtQ1csVUFBbEUsR0FBK0UsRUFBM0YsQ0FQaUIsQ0FRakI7O0FBQ0EsUUFBSUMsSUFBSSxHQUFHLEtBQUtoQixJQUFMLENBQVVZLEtBQVYsR0FBZ0IsQ0FBM0I7O0FBQ0EsUUFBSSxLQUFLbEMsWUFBTCxJQUFxQixDQUF6QixFQUE0QjtBQUN4Qm1DLE1BQUFBLEtBQUssR0FBRyxDQUFDSCxJQUFJLENBQUNDLE1BQUwsRUFBRCxHQUFpQkssSUFBekI7QUFDSCxLQUZELE1BRU87QUFDSEgsTUFBQUEsS0FBSyxHQUFHSCxJQUFJLENBQUNDLE1BQUwsS0FBZ0JLLElBQXhCO0FBQ0g7O0FBQ0QsU0FBS3RDLFlBQUwsR0FBb0JtQyxLQUFwQixDQWZpQixDQWdCakI7O0FBQ0EsV0FBTy9ELEVBQUUsQ0FBQzRDLEVBQUgsQ0FBTW1CLEtBQU4sRUFBYUMsS0FBYixDQUFQO0FBQ0gsR0FqSmU7QUFrSmhCRyxFQUFBQSxTQWxKZ0IscUJBa0pOQyxHQWxKTSxFQWtKRDtBQUNYLFNBQUtDLEtBQUwsSUFBYyxDQUFkLENBRFcsQ0FFWDs7QUFDQSxTQUFLeEQsWUFBTCxDQUFrQnNCLE1BQWxCLEdBQTJCLFlBQVksS0FBS2tDLEtBQUwsQ0FBV0MsUUFBWCxFQUF2QyxDQUhXLENBSVg7O0FBQ0EsUUFBSUMsRUFBRSxHQUFHLEtBQUtDLFlBQUwsRUFBVDtBQUNBLFNBQUt0QixJQUFMLENBQVVDLFFBQVYsQ0FBbUJvQixFQUFFLENBQUNyQixJQUF0QjtBQUNBcUIsSUFBQUEsRUFBRSxDQUFDckIsSUFBSCxDQUFRRSxXQUFSLENBQW9CZ0IsR0FBcEI7QUFDQUcsSUFBQUEsRUFBRSxDQUFDRSxJQUFILEdBUlcsQ0FTWDs7QUFDQXpFLElBQUFBLEVBQUUsQ0FBQzBFLFdBQUgsQ0FBZUMsVUFBZixDQUEwQixLQUFLNUQsVUFBL0IsRUFBMkMsS0FBM0M7QUFDSCxHQTdKZTtBQThKaEI2RCxFQUFBQSxNQTlKZ0Isa0JBOEpSQyxFQTlKUSxFQThKSjtBQUNSO0FBQ0E7QUFDQSxRQUFJLEtBQUtoRCxLQUFMLEdBQWEsS0FBS0MsWUFBdEIsRUFBb0M7QUFDaEMsV0FBS2dELFFBQUw7QUFDQSxXQUFLL0MsT0FBTCxHQUFlLEtBQWYsQ0FGZ0MsQ0FFUjs7QUFDeEI7QUFDSDs7QUFDRCxTQUFLRixLQUFMLElBQWNnRCxFQUFkO0FBQ0gsR0F2S2U7QUF3S2hCckMsRUFBQUEsVUFBVSxFQUFFLHNCQUFZO0FBQ3BCLFNBQUs2QixLQUFMLEdBQWEsQ0FBYjtBQUNBLFNBQUt4RCxZQUFMLENBQWtCc0IsTUFBbEIsR0FBMkIsWUFBWSxLQUFLa0MsS0FBTCxDQUFXQyxRQUFYLEVBQXZDO0FBQ0gsR0EzS2U7QUE0S2hCRSxFQUFBQSxZQTVLZ0IsMEJBNEtBO0FBQ1osUUFBSUQsRUFBSjs7QUFDQSxRQUFJLEtBQUtqQyxTQUFMLENBQWVTLElBQWYsS0FBd0IsQ0FBNUIsRUFBK0I7QUFDM0J3QixNQUFBQSxFQUFFLEdBQUcsS0FBS2pDLFNBQUwsQ0FBZVUsR0FBZixFQUFMO0FBQ0EsYUFBT3VCLEVBQUUsQ0FBQ2pCLFlBQUgsQ0FBZ0IsU0FBaEIsQ0FBUDtBQUNILEtBSEQsTUFHTztBQUNIaUIsTUFBQUEsRUFBRSxHQUFHdkUsRUFBRSxDQUFDaUQsV0FBSCxDQUFlLEtBQUsxQyxhQUFwQixFQUFtQytDLFlBQW5DLENBQWdELFNBQWhELENBQUw7QUFDQWlCLE1BQUFBLEVBQUUsQ0FBQ2hCLElBQUgsQ0FBUSxJQUFSO0FBQ0EsYUFBT2dCLEVBQVA7QUFDSDtBQUNKLEdBdExlO0FBdUxoQlEsRUFBQUEsY0F2TGdCLDBCQXVMQUMsT0F2TEEsRUF1TFM7QUFDckIsU0FBSzFDLFNBQUwsQ0FBZXFCLEdBQWYsQ0FBbUJxQixPQUFuQjtBQUNILEdBekxlO0FBMExoQkYsRUFBQUEsUUExTGdCLHNCQTBMTDtBQUNQLFNBQUs1RCxZQUFMLENBQWtCd0IsTUFBbEIsR0FBMkIsSUFBM0I7QUFDQSxTQUFLOUIsTUFBTCxDQUFZbUIsT0FBWixHQUFzQixLQUF0QjtBQUNBLFNBQUtuQixNQUFMLENBQVlxRSxRQUFaO0FBQ0EsU0FBS3RELFdBQUwsQ0FBaUJ1RCxPQUFqQjtBQUNBLFNBQUtqRSxPQUFMLENBQWF3QixDQUFiLEdBQWlCLENBQWpCO0FBQ0g7QUFoTWUsQ0FBVCxDQUFYIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuY29uc3QgUGxheWVyID0gcmVxdWlyZSgnUGxheWVyJyk7XHJcbmNvbnN0IFNjb3JlRlggPSByZXF1aXJlKCdTY29yZUZYJyk7XHJcbmNvbnN0IFN0YXIgPSByZXF1aXJlKCdTdGFyJyk7XHJcbnZhciBHYW1lID0gY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgLy8g6L+Z5Liq5bGe5oCn5byV55So5LqG5pif5pif6aKE572u6LWE5rqQXHJcbiAgICAgIHN0YXJQcmVmYWI6IHtcclxuICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICB0eXBlOiBjYy5QcmVmYWJcclxuICAgICAgfSxcclxuICAgICAgc2NvcmVGWFByZWZhYjoge1xyXG4gICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgdHlwZTogY2MuUHJlZmFiXHJcbiAgICAgIH0sXHJcbiAgICAgIC8vIOaYn+aYn+S6p+eUn+WQjua2iOWkseaXtumXtOeahOmaj+acuuiMg+WbtFxyXG4gICAgICBtYXhTdGFyRHVyYXRpb246IDAsXHJcbiAgICAgIG1pblN0YXJEdXJhdGlvbjogMCxcclxuXHJcbiAgICAgIC8vIOWcsOmdouiKgueCue+8jOeUqOS6juehruWumuaYn+aYn+eUn+aIkOeahOmrmOW6plxyXG4gICAgICBncm91bmQ6IHtcclxuICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICB0eXBlOiBjYy5Ob2RlXHJcbiAgICAgIH0sXHJcblxyXG4gICAgICAvLyBQbGF5ZXLoioLngrnvvIznlKjkuo7ojrflj5bkuLvop5LlvLnot7PnmoTpq5jluqblkozmjqfliLbkuLvop5LooYzliqjlvIDlhbNcclxuICAgICAgcGxheWVyOiB7XHJcbiAgICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgICAgICAgdHlwZTogUGxheWVyXHJcbiAgICAgIH0sXHJcblxyXG4gICAgICAvLyBzY29yZSBsYWJlbCDnmoTlvJXnlKhcclxuICAgICAgc2NvcmVEaXNwbGF5OiB7XHJcbiAgICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgICAgICAgdHlwZTogY2MuTGFiZWxcclxuICAgICAgfSxcclxuICAgICAgLy8g5b6X5YiG6Z+z5pWI6LWE5rqQXHJcbiAgICAgIHNjb3JlQXVkaW86IHtcclxuICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXAgICAgICBcclxuICAgICAgfSxcclxuICAgICAgYnRuTm9kZToge1xyXG4gICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgdHlwZTogY2MuTm9kZVxyXG4gICAgICB9LFxyXG4gICAgICBnYW1lT3Zlck5vZGU6IHtcclxuICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgICAgIHR5cGU6IGNjLk5vZGVcclxuICAgICAgfSxcclxuICAgICAgY29udHJvbEhpbnRMYWJlbDoge1xyXG4gICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgdHlwZTogY2MuTGFiZWxcclxuICAgICAgfSxcclxuICAgICAga2V5Ym9hcmRIaW50OiB7XHJcbiAgICAgICAgZGVmYXVsdDogJycsXHJcbiAgICAgICAgbXVsdGlsaW5lOiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIHRvdWNoSGludDoge1xyXG4gICAgICAgIGRlZmF1bHQ6ICcnLFxyXG4gICAgICAgIG11bHRpbGluZTogdHJ1ZVxyXG4gICAgICB9LFxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgICAgIC8vIOiOt+WPluWcsOW5s+mdoueahHnovbTlnZDmoIdcclxuICAgICAgICB0aGlzLmdyb3VuZFkgPSB0aGlzLmdyb3VuZC55ICsgdGhpcy5ncm91bmQuaGVpZ2h0LzI7XHJcbiAgICAgICAgLy8gc3RvcmUgbGFzdCBzdGFyJ3MgeCBwb3NpdGlvblxyXG4gICAgICAgIHRoaXMuY3VycmVudFN0YXIgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFN0YXJYID0gMDtcclxuXHJcbiAgICAgICAgLy8g5Yid5aeL5YyW6K6h5pe25ZmoXHJcbiAgICAgICAgdGhpcy50aW1lciA9IDA7XHJcbiAgICAgICAgdGhpcy5zdGFyRHVyYXRpb24gPSAwO1xyXG5cclxuICAgICAgICAvLyBpcyBzaG93aW5nIG1lbnUgb3IgcnVubmluZyBnYW1lXHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gZmFsc2U7XHJcbiAgICAgICAgLy8gaW5pdGlhbGl6ZSBjb250cm9sIGhpbnRcclxuICAgICAgICB2YXIgaGludFRleHQgPSBjYy5zeXMuaXNNb2JpbGUgPyB0aGlzLnRvdWNoSGludCA6IHRoaXMua2V5Ym9hcmRIaW50O1xyXG4gICAgICAgIHRoaXMuY29udHJvbEhpbnRMYWJlbC5zdHJpbmcgPSBoaW50VGV4dDtcclxuXHJcbiAgICAgICAgLy8gaW5pdGlhbGl6ZSBzdGFyIGFuZCBzY29yZSBwb29sXHJcbiAgICAgICAgdGhpcy5zdGFyUG9vbCA9IG5ldyBjYy5Ob2RlUG9vbCgnU3RhcicpO1xyXG4gICAgICAgIHRoaXMuc2NvcmVQb29sID0gbmV3IGNjLk5vZGVQb29sKCdTY29yZUZYJyk7XHJcbiAgICAgICAgXHJcblxyXG4gICAgICAgIFxyXG4gICAgfSxcclxuICAgIG9uU3RhcnRHYW1lICgpIHtcclxuICAgICAgICAvLyDliJ3lp4vljJborqHliIZcclxuICAgICAgICB0aGlzLnJlc2V0U2NvcmUoKTtcclxuICAgICAgICAvLyBzZXQgZ2FtZSBzdGF0ZSB0byBydW5uaW5nXHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICAvLyBzZXQgYnV0dG9uIGFuZCBnYW1lb3ZlciB0ZXh0IG91dCBvZiBzY3JlZW5cclxuICAgICAgICB0aGlzLmJ0bk5vZGUueCA9IDMwMDA7XHJcbiAgICAgICAgdGhpcy5nYW1lT3Zlck5vZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgLy8gcmVzZXQgcGxheWVyIHBvc2l0aW9uIGFuZCBtb3ZlIHNwZWVkXHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuc3RhcnRNb3ZlQXQoY2MudjIoMCwgdGhpcy5ncm91bmRZKSk7XHJcbiAgICAgICAgLy8gc3Bhd24gc3RhclxyXG4gICAgICAgIHRoaXMuc3Bhd25OZXdTdGFyKCk7XHJcbiAgICB9LFxyXG4gICAgc3Bhd25OZXdTdGFyKCkge1xyXG4gICAgICAgIHZhciBuZXdTdGFyID0gbnVsbDtcclxuICAgICAgICAvLyDkvb/nlKjnu5nlrprnmoTmqKHmnb/lnKjlnLrmma/kuK3nlJ/miJDkuIDkuKrmlrDoioLngrlcclxuICAgICAgICBpZiAodGhpcy5zdGFyUG9vbC5zaXplKCkgPiAwKSB7XHJcbiAgICAgICAgICAgIG5ld1N0YXIgPSB0aGlzLnN0YXJQb29sLmdldCh0aGlzKTsgLy8gdGhpcyB3aWxsIGJlIHBhc3NlZCB0byBTdGFyJ3MgcmV1c2UgbWV0aG9kXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbmV3U3RhciA9IGNjLmluc3RhbnRpYXRlKHRoaXMuc3RhclByZWZhYik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIOWwhuaWsOWinueahOiKgueCuea3u+WKoOWIsENhbnZhc+iKgueCueS4i+mdolxyXG4gICAgICAgIHRoaXMubm9kZS5hZGRDaGlsZChuZXdTdGFyKTtcclxuICAgICAgICAvLyDkuLrmmJ/mmJ/orr7nva7kuIDkuKrpmo/mnLrkvY3nva5cclxuICAgICAgICBuZXdTdGFyLnNldFBvc2l0aW9uKHRoaXMuZ2V0TmV3U3RhclBvc2l0aW9uKCkpXHJcbiAgICAgICAgLy8g5Zyo5pif5pif6ISa5pys57uE5Lu25LiK5L+d5a2YR2FtZeWvueixoeeahOW8leeUqFxyXG4gICAgICAgIG5ld1N0YXIuZ2V0Q29tcG9uZW50KCdTdGFyJykuaW5pdCh0aGlzKTtcclxuICAgICAgICAvLyBzdGFydCBzdGFyIHRpbWVyIGFuZCBzdG9yZSBzdGFyIHJlZmVyZW5jZVxyXG4gICAgICAgIHRoaXMuc3RhcnRUaW1lcigpO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFN0YXIgPSBuZXdTdGFyO1xyXG4gICAgfSxcclxuICAgIGRlc3Bhd25TdGFyIChzdGFyKSB7XHJcbiAgICAgICAgdGhpcy5zdGFyUG9vbC5wdXQoc3Rhcik7XHJcbiAgICAgICAgdGhpcy5zcGF3bk5ld1N0YXIoKTtcclxuICAgIH0sXHJcbiAgICBzdGFydFRpbWVyICgpIHtcclxuICAgICAgICAvLyBnZXQgYSBsaWZlIGR1cmF0aW9uIGZvciBuZXh0IHN0YXJcclxuICAgICAgICB0aGlzLnN0YXJEdXJhdGlvbiA9IHRoaXMubWluU3RhckR1cmF0aW9uICsgTWF0aC5yYW5kb20oKSAqICh0aGlzLm1heFN0YXJEdXJhdGlvbiAtIHRoaXMubWluU3RhckR1cmF0aW9uKTtcclxuICAgICAgICB0aGlzLnRpbWVyID0gMDtcclxuICAgIH0sXHJcbiAgICBnZXROZXdTdGFyUG9zaXRpb24oKSB7XHJcbiAgICAgICAgLy8gaWYgdGhlcmUncyBubyBzdGFyLCBzZXQgYSByYW5kb20geCBwb3NcclxuICAgICAgICBpZiAoIXRoaXMuY3VycmVudFN0YXIpIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50U3RhclggPSAoTWF0aC5yYW5kb20oKSAtIDAuNSkgKiAyICogdGhpcy5ub2RlLndpZHRoLzI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciByYW5kWCA9IDA7XHJcbiAgICAgICAgLy8g5qC55o2u5Zyw5bmz6Z2i5L2N572u5ZKM5Li76KeS6Lez6LeD6auY5bqm77yM6ZqP5py65b6X5Yiw5LiA5Liq5pif5pif55qEeeWdkOagh1xyXG4gICAgICAgIHZhciByYW5kWSA9IHRoaXMuZ3JvdW5kWSArIE1hdGgucmFuZG9tKCkgKiB0aGlzLnBsYXllci5nZXRDb21wb25lbnQoJ1BsYXllcicpLmp1bXBIZWlnaHQgKyA1MDtcclxuICAgICAgICAvLyDmoLnmja7lsY/luZXlrr3luqbvvIzpmo/mnLrlvpfliLDkuIDkuKrmmJ/mmJ945Z2Q5qCHXHJcbiAgICAgICAgdmFyIG1heFggPSB0aGlzLm5vZGUud2lkdGgvMjtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50U3RhclggPj0gMCkge1xyXG4gICAgICAgICAgICByYW5kWCA9IC1NYXRoLnJhbmRvbSgpICogbWF4WDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByYW5kWCA9IE1hdGgucmFuZG9tKCkgKiBtYXhYO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmN1cnJlbnRTdGFyWCA9IHJhbmRYO1xyXG4gICAgICAgIC8vIOi/lOWbnuaYn+aYn+WdkOagh1xyXG4gICAgICAgIHJldHVybiBjYy52MihyYW5kWCwgcmFuZFkpO1xyXG4gICAgfSxcclxuICAgIGdhaW5TY29yZShwb3MpIHtcclxuICAgICAgICB0aGlzLnNjb3JlICs9IDE7XHJcbiAgICAgICAgLy8g5pu05pawIHNjb3JlRGlzcGxheSBMYWJlbCDnmoTmloflrZdcclxuICAgICAgICB0aGlzLnNjb3JlRGlzcGxheS5zdHJpbmcgPSAnU2NvcmU6ICcgKyB0aGlzLnNjb3JlLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgLy8g5pKt5pS+54m55pWIXHJcbiAgICAgICAgdmFyIGZ4ID0gdGhpcy5zcGF3blNjb3JlRlgoKTtcclxuICAgICAgICB0aGlzLm5vZGUuYWRkQ2hpbGQoZngubm9kZSk7XHJcbiAgICAgICAgZngubm9kZS5zZXRQb3NpdGlvbihwb3MpO1xyXG4gICAgICAgIGZ4LnBsYXkoKTtcclxuICAgICAgICAvLyDmkq3mlL7lvpfliIbpn7PmlYhcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHRoaXMuc2NvcmVBdWRpbywgZmFsc2UpO1xyXG4gICAgfSxcclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICAvLyDmr4/luKfmm7TmlrDorqHml7blmajvvIzotoXov4fpmZDluqbov5jmsqHmnInnlJ/miJDmlrDnmoTmmJ/mmJ9cclxuICAgICAgICAvLyDlsLHkvJrosIPnlKjmuLjmiI/lpLHotKXpgLvovpFcclxuICAgICAgICBpZiAodGhpcy50aW1lciA+IHRoaXMuc3RhckR1cmF0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZU92ZXIoKTtcclxuICAgICAgICAgICAgdGhpcy5lbmFibGVkID0gZmFsc2U7ICAgLy8gZGlzYWJsZSB0aGlzIHRvIGF2b2lkIGdhbWVPdmVyKCkgcmVwZWF0ZWRseVxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudGltZXIgKz0gZHQ7XHJcbiAgICB9LFxyXG4gICAgcmVzZXRTY29yZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuc2NvcmUgPSAwO1xyXG4gICAgICAgIHRoaXMuc2NvcmVEaXNwbGF5LnN0cmluZyA9ICdTY29yZTogJyArIHRoaXMuc2NvcmUudG9TdHJpbmcoKTtcclxuICAgIH0sXHJcbiAgICBzcGF3blNjb3JlRlggKCkge1xyXG4gICAgICAgIHZhciBmeDtcclxuICAgICAgICBpZiAodGhpcy5zY29yZVBvb2wuc2l6ZSgpID4gMCkge1xyXG4gICAgICAgICAgICBmeCA9IHRoaXMuc2NvcmVQb29sLmdldCgpO1xyXG4gICAgICAgICAgICByZXR1cm4gZnguZ2V0Q29tcG9uZW50KCdTY29yZUZYJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZnggPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnNjb3JlRlhQcmVmYWIpLmdldENvbXBvbmVudCgnU2NvcmVGWCcpO1xyXG4gICAgICAgICAgICBmeC5pbml0KHRoaXMpO1xyXG4gICAgICAgICAgICByZXR1cm4gZng7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRlc3Bhd25TY29yZUZYIChzY29yZUZYKSB7XHJcbiAgICAgICAgdGhpcy5zY29yZVBvb2wucHV0KHNjb3JlRlgpO1xyXG4gICAgfSxcclxuICAgIGdhbWVPdmVyKCkge1xyXG4gICAgICAgIHRoaXMuZ2FtZU92ZXJOb2RlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMucGxheWVyLnN0b3BNb3ZlKCk7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50U3Rhci5kZXN0cm95KCk7XHJcbiAgICAgICAgdGhpcy5idG5Ob2RlLnggPSAwO1xyXG4gICAgfVxyXG59KTtcclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/ScoreAnim.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd7fc9pg32dHz7XdDreD7dSy', 'ScoreAnim');
// scripts/ScoreAnim.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {},
  init: function init(scoreFX) {
    this.scoreFX = scoreFX;
  },
  hideFX: function hideFX() {
    this.scoreFX.despawn();
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU2NvcmVBbmltLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiaW5pdCIsInNjb3JlRlgiLCJoaWRlRlgiLCJkZXNwYXduIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUUsRUFIUDtBQU9MQyxFQUFBQSxJQVBLLGdCQU9BQyxPQVBBLEVBT1M7QUFDVixTQUFLQSxPQUFMLEdBQWVBLE9BQWY7QUFDSCxHQVRJO0FBVUxDLEVBQUFBLE1BVkssb0JBVUk7QUFDTCxTQUFLRCxPQUFMLENBQWFFLE9BQWI7QUFDSDtBQVpJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9jbGFzcy5odG1sXHJcbi8vIExlYXJuIEF0dHJpYnV0ZTpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgIFxyXG4gICAgfSxcclxuXHJcbiAgICBpbml0KHNjb3JlRlgpIHtcclxuICAgICAgICB0aGlzLnNjb3JlRlggPSBzY29yZUZYO1xyXG4gICAgfSxcclxuICAgIGhpZGVGWCgpIHtcclxuICAgICAgICB0aGlzLnNjb3JlRlguZGVzcGF3bigpO1xyXG4gICAgfVxyXG59KTtcclxuIl19
//------QC-SOURCE-SPLIT------

                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/ScoreFX.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '53983Bn/u1AxLjU2OZoATPB', 'ScoreFX');
// scripts/ScoreFX.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    anim: {
      "default": null,
      type: cc.Animation
    }
  },
  init: function init(game) {
    this.game = game;
    this.anim.getComponent('ScoreAnim').init(this);
  },
  despawn: function despawn() {
    this.game.despawnScoreFX(this.node);
  },
  play: function play() {
    this.anim.play('score_pop');
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU2NvcmVGWC5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImFuaW0iLCJ0eXBlIiwiQW5pbWF0aW9uIiwiaW5pdCIsImdhbWUiLCJnZXRDb21wb25lbnQiLCJkZXNwYXduIiwiZGVzcGF3blNjb3JlRlgiLCJub2RlIiwicGxheSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLElBQUksRUFBRTtBQUNGLGlCQUFTLElBRFA7QUFFRkMsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNNO0FBRlA7QUFERSxHQUhQO0FBVUxDLEVBQUFBLElBVkssZ0JBVUFDLElBVkEsRUFVTTtBQUNQLFNBQUtBLElBQUwsR0FBWUEsSUFBWjtBQUNBLFNBQUtKLElBQUwsQ0FBVUssWUFBVixDQUF1QixXQUF2QixFQUFvQ0YsSUFBcEMsQ0FBeUMsSUFBekM7QUFDSCxHQWJJO0FBY0xHLEVBQUFBLE9BZEsscUJBY0s7QUFDTixTQUFLRixJQUFMLENBQVVHLGNBQVYsQ0FBeUIsS0FBS0MsSUFBOUI7QUFDSCxHQWhCSTtBQWlCTEMsRUFBQUEsSUFqQkssa0JBaUJFO0FBQ0gsU0FBS1QsSUFBTCxDQUFVUyxJQUFWLENBQWUsV0FBZjtBQUNIO0FBbkJJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9jbGFzcy5odG1sXHJcbi8vIExlYXJuIEF0dHJpYnV0ZTpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBhbmltOiB7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICAgIHR5cGU6IGNjLkFuaW1hdGlvblxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgaW5pdChnYW1lKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lID0gZ2FtZTtcclxuICAgICAgICB0aGlzLmFuaW0uZ2V0Q29tcG9uZW50KCdTY29yZUFuaW0nKS5pbml0KHRoaXMpO1xyXG4gICAgfSxcclxuICAgIGRlc3Bhd24oKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lLmRlc3Bhd25TY29yZUZYKHRoaXMubm9kZSk7XHJcbiAgICB9LFxyXG4gICAgcGxheSgpIHtcclxuICAgICAgICB0aGlzLmFuaW0ucGxheSgnc2NvcmVfcG9wJyk7XHJcbiAgICB9XHJcbn0pO1xyXG4iXX0=
//------QC-SOURCE-SPLIT------
