
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '2050d5nfIdKuZ7JojDiRfk2', 'Game');
// scripts/Game.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
var Player = require('Player');

var ScoreFX = require('ScoreFX');

var Star = require('Star');

var Game = cc.Class({
  "extends": cc.Component,
  properties: {
    // 这个属性引用了星星预置资源
    starPrefab: {
      "default": null,
      type: cc.Prefab
    },
    scoreFXPrefab: {
      "default": null,
      type: cc.Prefab
    },
    // 星星产生后消失时间的随机范围
    maxStarDuration: 0,
    minStarDuration: 0,
    // 地面节点，用于确定星星生成的高度
    ground: {
      "default": null,
      type: cc.Node
    },
    // Player节点，用于获取主角弹跳的高度和控制主角行动开关
    player: {
      "default": null,
      type: Player
    },
    // score label 的引用
    scoreDisplay: {
      "default": null,
      type: cc.Label
    },
    // 得分音效资源
    scoreAudio: {
      "default": null,
      type: cc.AudioClip
    },
    btnNode: {
      "default": null,
      type: cc.Node
    },
    gameOverNode: {
      "default": null,
      type: cc.Node
    },
    controlHintLabel: {
      "default": null,
      type: cc.Label
    },
    keyboardHint: {
      "default": '',
      multiline: true
    },
    touchHint: {
      "default": '',
      multiline: true
    }
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    // 获取地平面的y轴坐标
    this.groundY = this.ground.y + this.ground.height / 2; // store last star's x position

    this.currentStar = null;
    this.currentStarX = 0; // 初始化计时器

    this.timer = 0;
    this.starDuration = 0; // is showing menu or running game

    this.enabled = false; // initialize control hint

    var hintText = cc.sys.isMobile ? this.touchHint : this.keyboardHint;
    this.controlHintLabel.string = hintText; // initialize star and score pool

    this.starPool = new cc.NodePool('Star');
    this.scorePool = new cc.NodePool('ScoreFX');
  },
  onStartGame: function onStartGame() {
    // 初始化计分
    this.resetScore(); // set game state to running

    this.enabled = true; // set button and gameover text out of screen

    this.btnNode.x = 3000;
    this.gameOverNode.active = false; // reset player position and move speed

    this.player.startMoveAt(cc.v2(0, this.groundY)); // spawn star

    this.spawnNewStar();
  },
  spawnNewStar: function spawnNewStar() {
    var newStar = null; // 使用给定的模板在场景中生成一个新节点

    if (this.starPool.size() > 0) {
      newStar = this.starPool.get(this); // this will be passed to Star's reuse method
    } else {
      newStar = cc.instantiate(this.starPrefab);
    } // 将新增的节点添加到Canvas节点下面


    this.node.addChild(newStar); // 为星星设置一个随机位置

    newStar.setPosition(this.getNewStarPosition()); // 在星星脚本组件上保存Game对象的引用

    newStar.getComponent('Star').init(this); // start star timer and store star reference

    this.startTimer();
    this.currentStar = newStar;
  },
  despawnStar: function despawnStar(star) {
    this.starPool.put(star);
    this.spawnNewStar();
  },
  startTimer: function startTimer() {
    // get a life duration for next star
    this.starDuration = this.minStarDuration + Math.random() * (this.maxStarDuration - this.minStarDuration);
    this.timer = 0;
  },
  getNewStarPosition: function getNewStarPosition() {
    // if there's no star, set a random x pos
    if (!this.currentStar) {
      this.currentStarX = (Math.random() - 0.5) * 2 * this.node.width / 2;
    }

    var randX = 0; // 根据地平面位置和主角跳跃高度，随机得到一个星星的y坐标

    var randY = this.groundY + Math.random() * this.player.getComponent('Player').jumpHeight + 50; // 根据屏幕宽度，随机得到一个星星x坐标

    var maxX = this.node.width / 2;

    if (this.currentStarX >= 0) {
      randX = -Math.random() * maxX;
    } else {
      randX = Math.random() * maxX;
    }

    this.currentStarX = randX; // 返回星星坐标

    return cc.v2(randX, randY);
  },
  gainScore: function gainScore(pos) {
    this.score += 1; // 更新 scoreDisplay Label 的文字

    this.scoreDisplay.string = 'Score: ' + this.score.toString(); // 播放特效

    var fx = this.spawnScoreFX();
    this.node.addChild(fx.node);
    fx.node.setPosition(pos);
    fx.play(); // 播放得分音效

    cc.audioEngine.playEffect(this.scoreAudio, false);
  },
  update: function update(dt) {
    // 每帧更新计时器，超过限度还没有生成新的星星
    // 就会调用游戏失败逻辑
    if (this.timer > this.starDuration) {
      this.gameOver();
      this.enabled = false; // disable this to avoid gameOver() repeatedly

      return;
    }

    this.timer += dt;
  },
  resetScore: function resetScore() {
    this.score = 0;
    this.scoreDisplay.string = 'Score: ' + this.score.toString();
  },
  spawnScoreFX: function spawnScoreFX() {
    var fx;

    if (this.scorePool.size() > 0) {
      fx = this.scorePool.get();
      return fx.getComponent('ScoreFX');
    } else {
      fx = cc.instantiate(this.scoreFXPrefab).getComponent('ScoreFX');
      fx.init(this);
      return fx;
    }
  },
  despawnScoreFX: function despawnScoreFX(scoreFX) {
    this.scorePool.put(scoreFX);
  },
  gameOver: function gameOver() {
    this.gameOverNode.active = true;
    this.player.enabled = false;
    this.player.stopMove();
    this.currentStar.destroy();
    this.btnNode.x = 0;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcR2FtZS5qcyJdLCJuYW1lcyI6WyJQbGF5ZXIiLCJyZXF1aXJlIiwiU2NvcmVGWCIsIlN0YXIiLCJHYW1lIiwiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJzdGFyUHJlZmFiIiwidHlwZSIsIlByZWZhYiIsInNjb3JlRlhQcmVmYWIiLCJtYXhTdGFyRHVyYXRpb24iLCJtaW5TdGFyRHVyYXRpb24iLCJncm91bmQiLCJOb2RlIiwicGxheWVyIiwic2NvcmVEaXNwbGF5IiwiTGFiZWwiLCJzY29yZUF1ZGlvIiwiQXVkaW9DbGlwIiwiYnRuTm9kZSIsImdhbWVPdmVyTm9kZSIsImNvbnRyb2xIaW50TGFiZWwiLCJrZXlib2FyZEhpbnQiLCJtdWx0aWxpbmUiLCJ0b3VjaEhpbnQiLCJvbkxvYWQiLCJncm91bmRZIiwieSIsImhlaWdodCIsImN1cnJlbnRTdGFyIiwiY3VycmVudFN0YXJYIiwidGltZXIiLCJzdGFyRHVyYXRpb24iLCJlbmFibGVkIiwiaGludFRleHQiLCJzeXMiLCJpc01vYmlsZSIsInN0cmluZyIsInN0YXJQb29sIiwiTm9kZVBvb2wiLCJzY29yZVBvb2wiLCJvblN0YXJ0R2FtZSIsInJlc2V0U2NvcmUiLCJ4IiwiYWN0aXZlIiwic3RhcnRNb3ZlQXQiLCJ2MiIsInNwYXduTmV3U3RhciIsIm5ld1N0YXIiLCJzaXplIiwiZ2V0IiwiaW5zdGFudGlhdGUiLCJub2RlIiwiYWRkQ2hpbGQiLCJzZXRQb3NpdGlvbiIsImdldE5ld1N0YXJQb3NpdGlvbiIsImdldENvbXBvbmVudCIsImluaXQiLCJzdGFydFRpbWVyIiwiZGVzcGF3blN0YXIiLCJzdGFyIiwicHV0IiwiTWF0aCIsInJhbmRvbSIsIndpZHRoIiwicmFuZFgiLCJyYW5kWSIsImp1bXBIZWlnaHQiLCJtYXhYIiwiZ2FpblNjb3JlIiwicG9zIiwic2NvcmUiLCJ0b1N0cmluZyIsImZ4Iiwic3Bhd25TY29yZUZYIiwicGxheSIsImF1ZGlvRW5naW5lIiwicGxheUVmZmVjdCIsInVwZGF0ZSIsImR0IiwiZ2FtZU92ZXIiLCJkZXNwYXduU2NvcmVGWCIsInNjb3JlRlgiLCJzdG9wTW92ZSIsImRlc3Ryb3kiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBTUEsTUFBTSxHQUFHQyxPQUFPLENBQUMsUUFBRCxDQUF0Qjs7QUFDQSxJQUFNQyxPQUFPLEdBQUdELE9BQU8sQ0FBQyxTQUFELENBQXZCOztBQUNBLElBQU1FLElBQUksR0FBR0YsT0FBTyxDQUFDLE1BQUQsQ0FBcEI7O0FBQ0EsSUFBSUcsSUFBSSxHQUFHQyxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNoQixhQUFTRCxFQUFFLENBQUNFLFNBREk7QUFHaEJDLEVBQUFBLFVBQVUsRUFBRTtBQUNWO0FBQ0FDLElBQUFBLFVBQVUsRUFBRTtBQUNSLGlCQUFTLElBREQ7QUFFUkMsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNNO0FBRkQsS0FGRjtBQU1WQyxJQUFBQSxhQUFhLEVBQUU7QUFDYixpQkFBUyxJQURJO0FBRWJGLE1BQUFBLElBQUksRUFBRUwsRUFBRSxDQUFDTTtBQUZJLEtBTkw7QUFVVjtBQUNBRSxJQUFBQSxlQUFlLEVBQUUsQ0FYUDtBQVlWQyxJQUFBQSxlQUFlLEVBQUUsQ0FaUDtBQWNWO0FBQ0FDLElBQUFBLE1BQU0sRUFBRTtBQUNKLGlCQUFTLElBREw7QUFFSkwsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNXO0FBRkwsS0FmRTtBQW9CVjtBQUNBQyxJQUFBQSxNQUFNLEVBQUU7QUFDSixpQkFBUyxJQURMO0FBRUpQLE1BQUFBLElBQUksRUFBRVY7QUFGRixLQXJCRTtBQTBCVjtBQUNBa0IsSUFBQUEsWUFBWSxFQUFFO0FBQ1YsaUJBQVMsSUFEQztBQUVWUixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ2M7QUFGQyxLQTNCSjtBQStCVjtBQUNBQyxJQUFBQSxVQUFVLEVBQUU7QUFDUixpQkFBUyxJQUREO0FBRVJWLE1BQUFBLElBQUksRUFBRUwsRUFBRSxDQUFDZ0I7QUFGRCxLQWhDRjtBQW9DVkMsSUFBQUEsT0FBTyxFQUFFO0FBQ1AsaUJBQVMsSUFERjtBQUVQWixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1c7QUFGRixLQXBDQztBQXdDVk8sSUFBQUEsWUFBWSxFQUFFO0FBQ1osaUJBQVMsSUFERztBQUVaYixNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ1c7QUFGRyxLQXhDSjtBQTRDVlEsSUFBQUEsZ0JBQWdCLEVBQUU7QUFDaEIsaUJBQVMsSUFETztBQUVoQmQsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNjO0FBRk8sS0E1Q1I7QUFnRFZNLElBQUFBLFlBQVksRUFBRTtBQUNaLGlCQUFTLEVBREc7QUFFWkMsTUFBQUEsU0FBUyxFQUFFO0FBRkMsS0FoREo7QUFvRFZDLElBQUFBLFNBQVMsRUFBRTtBQUNULGlCQUFTLEVBREE7QUFFVEQsTUFBQUEsU0FBUyxFQUFFO0FBRkY7QUFwREQsR0FISTtBQTZEaEI7QUFFQUUsRUFBQUEsTUEvRGdCLG9CQStETjtBQUNOO0FBQ0EsU0FBS0MsT0FBTCxHQUFlLEtBQUtkLE1BQUwsQ0FBWWUsQ0FBWixHQUFnQixLQUFLZixNQUFMLENBQVlnQixNQUFaLEdBQW1CLENBQWxELENBRk0sQ0FHTjs7QUFDQSxTQUFLQyxXQUFMLEdBQW1CLElBQW5CO0FBQ0EsU0FBS0MsWUFBTCxHQUFvQixDQUFwQixDQUxNLENBT047O0FBQ0EsU0FBS0MsS0FBTCxHQUFhLENBQWI7QUFDQSxTQUFLQyxZQUFMLEdBQW9CLENBQXBCLENBVE0sQ0FXTjs7QUFDQSxTQUFLQyxPQUFMLEdBQWUsS0FBZixDQVpNLENBYU47O0FBQ0EsUUFBSUMsUUFBUSxHQUFHaEMsRUFBRSxDQUFDaUMsR0FBSCxDQUFPQyxRQUFQLEdBQWtCLEtBQUtaLFNBQXZCLEdBQW1DLEtBQUtGLFlBQXZEO0FBQ0EsU0FBS0QsZ0JBQUwsQ0FBc0JnQixNQUF0QixHQUErQkgsUUFBL0IsQ0FmTSxDQWlCTjs7QUFDQSxTQUFLSSxRQUFMLEdBQWdCLElBQUlwQyxFQUFFLENBQUNxQyxRQUFQLENBQWdCLE1BQWhCLENBQWhCO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQixJQUFJdEMsRUFBRSxDQUFDcUMsUUFBUCxDQUFnQixTQUFoQixDQUFqQjtBQUlILEdBdEZlO0FBdUZoQkUsRUFBQUEsV0F2RmdCLHlCQXVGRDtBQUNYO0FBQ0EsU0FBS0MsVUFBTCxHQUZXLENBR1g7O0FBQ0EsU0FBS1QsT0FBTCxHQUFlLElBQWYsQ0FKVyxDQUtYOztBQUNBLFNBQUtkLE9BQUwsQ0FBYXdCLENBQWIsR0FBaUIsSUFBakI7QUFDQSxTQUFLdkIsWUFBTCxDQUFrQndCLE1BQWxCLEdBQTJCLEtBQTNCLENBUFcsQ0FRWDs7QUFDQSxTQUFLOUIsTUFBTCxDQUFZK0IsV0FBWixDQUF3QjNDLEVBQUUsQ0FBQzRDLEVBQUgsQ0FBTSxDQUFOLEVBQVMsS0FBS3BCLE9BQWQsQ0FBeEIsRUFUVyxDQVVYOztBQUNBLFNBQUtxQixZQUFMO0FBQ0gsR0FuR2U7QUFvR2hCQSxFQUFBQSxZQXBHZ0IsMEJBb0dEO0FBQ1gsUUFBSUMsT0FBTyxHQUFHLElBQWQsQ0FEVyxDQUVYOztBQUNBLFFBQUksS0FBS1YsUUFBTCxDQUFjVyxJQUFkLEtBQXVCLENBQTNCLEVBQThCO0FBQzFCRCxNQUFBQSxPQUFPLEdBQUcsS0FBS1YsUUFBTCxDQUFjWSxHQUFkLENBQWtCLElBQWxCLENBQVYsQ0FEMEIsQ0FDUztBQUN0QyxLQUZELE1BRU87QUFDSEYsTUFBQUEsT0FBTyxHQUFHOUMsRUFBRSxDQUFDaUQsV0FBSCxDQUFlLEtBQUs3QyxVQUFwQixDQUFWO0FBQ0gsS0FQVSxDQVFYOzs7QUFDQSxTQUFLOEMsSUFBTCxDQUFVQyxRQUFWLENBQW1CTCxPQUFuQixFQVRXLENBVVg7O0FBQ0FBLElBQUFBLE9BQU8sQ0FBQ00sV0FBUixDQUFvQixLQUFLQyxrQkFBTCxFQUFwQixFQVhXLENBWVg7O0FBQ0FQLElBQUFBLE9BQU8sQ0FBQ1EsWUFBUixDQUFxQixNQUFyQixFQUE2QkMsSUFBN0IsQ0FBa0MsSUFBbEMsRUFiVyxDQWNYOztBQUNBLFNBQUtDLFVBQUw7QUFDQSxTQUFLN0IsV0FBTCxHQUFtQm1CLE9BQW5CO0FBQ0gsR0FySGU7QUFzSGhCVyxFQUFBQSxXQXRIZ0IsdUJBc0hIQyxJQXRIRyxFQXNIRztBQUNmLFNBQUt0QixRQUFMLENBQWN1QixHQUFkLENBQWtCRCxJQUFsQjtBQUNBLFNBQUtiLFlBQUw7QUFDSCxHQXpIZTtBQTBIaEJXLEVBQUFBLFVBMUhnQix3QkEwSEY7QUFDVjtBQUNBLFNBQUsxQixZQUFMLEdBQW9CLEtBQUtyQixlQUFMLEdBQXVCbUQsSUFBSSxDQUFDQyxNQUFMLE1BQWlCLEtBQUtyRCxlQUFMLEdBQXVCLEtBQUtDLGVBQTdDLENBQTNDO0FBQ0EsU0FBS29CLEtBQUwsR0FBYSxDQUFiO0FBQ0gsR0E5SGU7QUErSGhCd0IsRUFBQUEsa0JBL0hnQixnQ0ErSEs7QUFDakI7QUFDQSxRQUFJLENBQUMsS0FBSzFCLFdBQVYsRUFBdUI7QUFDbkIsV0FBS0MsWUFBTCxHQUFvQixDQUFDZ0MsSUFBSSxDQUFDQyxNQUFMLEtBQWdCLEdBQWpCLElBQXdCLENBQXhCLEdBQTRCLEtBQUtYLElBQUwsQ0FBVVksS0FBdEMsR0FBNEMsQ0FBaEU7QUFDSDs7QUFDRCxRQUFJQyxLQUFLLEdBQUcsQ0FBWixDQUxpQixDQU1qQjs7QUFDQSxRQUFJQyxLQUFLLEdBQUcsS0FBS3hDLE9BQUwsR0FBZW9DLElBQUksQ0FBQ0MsTUFBTCxLQUFnQixLQUFLakQsTUFBTCxDQUFZMEMsWUFBWixDQUF5QixRQUF6QixFQUFtQ1csVUFBbEUsR0FBK0UsRUFBM0YsQ0FQaUIsQ0FRakI7O0FBQ0EsUUFBSUMsSUFBSSxHQUFHLEtBQUtoQixJQUFMLENBQVVZLEtBQVYsR0FBZ0IsQ0FBM0I7O0FBQ0EsUUFBSSxLQUFLbEMsWUFBTCxJQUFxQixDQUF6QixFQUE0QjtBQUN4Qm1DLE1BQUFBLEtBQUssR0FBRyxDQUFDSCxJQUFJLENBQUNDLE1BQUwsRUFBRCxHQUFpQkssSUFBekI7QUFDSCxLQUZELE1BRU87QUFDSEgsTUFBQUEsS0FBSyxHQUFHSCxJQUFJLENBQUNDLE1BQUwsS0FBZ0JLLElBQXhCO0FBQ0g7O0FBQ0QsU0FBS3RDLFlBQUwsR0FBb0JtQyxLQUFwQixDQWZpQixDQWdCakI7O0FBQ0EsV0FBTy9ELEVBQUUsQ0FBQzRDLEVBQUgsQ0FBTW1CLEtBQU4sRUFBYUMsS0FBYixDQUFQO0FBQ0gsR0FqSmU7QUFrSmhCRyxFQUFBQSxTQWxKZ0IscUJBa0pOQyxHQWxKTSxFQWtKRDtBQUNYLFNBQUtDLEtBQUwsSUFBYyxDQUFkLENBRFcsQ0FFWDs7QUFDQSxTQUFLeEQsWUFBTCxDQUFrQnNCLE1BQWxCLEdBQTJCLFlBQVksS0FBS2tDLEtBQUwsQ0FBV0MsUUFBWCxFQUF2QyxDQUhXLENBSVg7O0FBQ0EsUUFBSUMsRUFBRSxHQUFHLEtBQUtDLFlBQUwsRUFBVDtBQUNBLFNBQUt0QixJQUFMLENBQVVDLFFBQVYsQ0FBbUJvQixFQUFFLENBQUNyQixJQUF0QjtBQUNBcUIsSUFBQUEsRUFBRSxDQUFDckIsSUFBSCxDQUFRRSxXQUFSLENBQW9CZ0IsR0FBcEI7QUFDQUcsSUFBQUEsRUFBRSxDQUFDRSxJQUFILEdBUlcsQ0FTWDs7QUFDQXpFLElBQUFBLEVBQUUsQ0FBQzBFLFdBQUgsQ0FBZUMsVUFBZixDQUEwQixLQUFLNUQsVUFBL0IsRUFBMkMsS0FBM0M7QUFDSCxHQTdKZTtBQThKaEI2RCxFQUFBQSxNQTlKZ0Isa0JBOEpSQyxFQTlKUSxFQThKSjtBQUNSO0FBQ0E7QUFDQSxRQUFJLEtBQUtoRCxLQUFMLEdBQWEsS0FBS0MsWUFBdEIsRUFBb0M7QUFDaEMsV0FBS2dELFFBQUw7QUFDQSxXQUFLL0MsT0FBTCxHQUFlLEtBQWYsQ0FGZ0MsQ0FFUjs7QUFDeEI7QUFDSDs7QUFDRCxTQUFLRixLQUFMLElBQWNnRCxFQUFkO0FBQ0gsR0F2S2U7QUF3S2hCckMsRUFBQUEsVUFBVSxFQUFFLHNCQUFZO0FBQ3BCLFNBQUs2QixLQUFMLEdBQWEsQ0FBYjtBQUNBLFNBQUt4RCxZQUFMLENBQWtCc0IsTUFBbEIsR0FBMkIsWUFBWSxLQUFLa0MsS0FBTCxDQUFXQyxRQUFYLEVBQXZDO0FBQ0gsR0EzS2U7QUE0S2hCRSxFQUFBQSxZQTVLZ0IsMEJBNEtBO0FBQ1osUUFBSUQsRUFBSjs7QUFDQSxRQUFJLEtBQUtqQyxTQUFMLENBQWVTLElBQWYsS0FBd0IsQ0FBNUIsRUFBK0I7QUFDM0J3QixNQUFBQSxFQUFFLEdBQUcsS0FBS2pDLFNBQUwsQ0FBZVUsR0FBZixFQUFMO0FBQ0EsYUFBT3VCLEVBQUUsQ0FBQ2pCLFlBQUgsQ0FBZ0IsU0FBaEIsQ0FBUDtBQUNILEtBSEQsTUFHTztBQUNIaUIsTUFBQUEsRUFBRSxHQUFHdkUsRUFBRSxDQUFDaUQsV0FBSCxDQUFlLEtBQUsxQyxhQUFwQixFQUFtQytDLFlBQW5DLENBQWdELFNBQWhELENBQUw7QUFDQWlCLE1BQUFBLEVBQUUsQ0FBQ2hCLElBQUgsQ0FBUSxJQUFSO0FBQ0EsYUFBT2dCLEVBQVA7QUFDSDtBQUNKLEdBdExlO0FBdUxoQlEsRUFBQUEsY0F2TGdCLDBCQXVMQUMsT0F2TEEsRUF1TFM7QUFDckIsU0FBSzFDLFNBQUwsQ0FBZXFCLEdBQWYsQ0FBbUJxQixPQUFuQjtBQUNILEdBekxlO0FBMExoQkYsRUFBQUEsUUExTGdCLHNCQTBMTDtBQUNQLFNBQUs1RCxZQUFMLENBQWtCd0IsTUFBbEIsR0FBMkIsSUFBM0I7QUFDQSxTQUFLOUIsTUFBTCxDQUFZbUIsT0FBWixHQUFzQixLQUF0QjtBQUNBLFNBQUtuQixNQUFMLENBQVlxRSxRQUFaO0FBQ0EsU0FBS3RELFdBQUwsQ0FBaUJ1RCxPQUFqQjtBQUNBLFNBQUtqRSxPQUFMLENBQWF3QixDQUFiLEdBQWlCLENBQWpCO0FBQ0g7QUFoTWUsQ0FBVCxDQUFYIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyIvLyBMZWFybiBjYy5DbGFzczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvY2xhc3MuaHRtbFxyXG4vLyBMZWFybiBBdHRyaWJ1dGU6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL3JlZmVyZW5jZS9hdHRyaWJ1dGVzLmh0bWxcclxuLy8gTGVhcm4gbGlmZS1jeWNsZSBjYWxsYmFja3M6XHJcbi8vICAtIGh0dHBzOi8vZG9jcy5jb2Nvcy5jb20vY3JlYXRvci9tYW51YWwvZW4vc2NyaXB0aW5nL2xpZmUtY3ljbGUtY2FsbGJhY2tzLmh0bWxcclxuY29uc3QgUGxheWVyID0gcmVxdWlyZSgnUGxheWVyJyk7XHJcbmNvbnN0IFNjb3JlRlggPSByZXF1aXJlKCdTY29yZUZYJyk7XHJcbmNvbnN0IFN0YXIgPSByZXF1aXJlKCdTdGFyJyk7XHJcbnZhciBHYW1lID0gY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgLy8g6L+Z5Liq5bGe5oCn5byV55So5LqG5pif5pif6aKE572u6LWE5rqQXHJcbiAgICAgIHN0YXJQcmVmYWI6IHtcclxuICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICB0eXBlOiBjYy5QcmVmYWJcclxuICAgICAgfSxcclxuICAgICAgc2NvcmVGWFByZWZhYjoge1xyXG4gICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgdHlwZTogY2MuUHJlZmFiXHJcbiAgICAgIH0sXHJcbiAgICAgIC8vIOaYn+aYn+S6p+eUn+WQjua2iOWkseaXtumXtOeahOmaj+acuuiMg+WbtFxyXG4gICAgICBtYXhTdGFyRHVyYXRpb246IDAsXHJcbiAgICAgIG1pblN0YXJEdXJhdGlvbjogMCxcclxuXHJcbiAgICAgIC8vIOWcsOmdouiKgueCue+8jOeUqOS6juehruWumuaYn+aYn+eUn+aIkOeahOmrmOW6plxyXG4gICAgICBncm91bmQ6IHtcclxuICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICB0eXBlOiBjYy5Ob2RlXHJcbiAgICAgIH0sXHJcblxyXG4gICAgICAvLyBQbGF5ZXLoioLngrnvvIznlKjkuo7ojrflj5bkuLvop5LlvLnot7PnmoTpq5jluqblkozmjqfliLbkuLvop5LooYzliqjlvIDlhbNcclxuICAgICAgcGxheWVyOiB7XHJcbiAgICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgICAgICAgdHlwZTogUGxheWVyXHJcbiAgICAgIH0sXHJcblxyXG4gICAgICAvLyBzY29yZSBsYWJlbCDnmoTlvJXnlKhcclxuICAgICAgc2NvcmVEaXNwbGF5OiB7XHJcbiAgICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgICAgICAgdHlwZTogY2MuTGFiZWxcclxuICAgICAgfSxcclxuICAgICAgLy8g5b6X5YiG6Z+z5pWI6LWE5rqQXHJcbiAgICAgIHNjb3JlQXVkaW86IHtcclxuICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICB0eXBlOiBjYy5BdWRpb0NsaXAgICAgICBcclxuICAgICAgfSxcclxuICAgICAgYnRuTm9kZToge1xyXG4gICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgdHlwZTogY2MuTm9kZVxyXG4gICAgICB9LFxyXG4gICAgICBnYW1lT3Zlck5vZGU6IHtcclxuICAgICAgICBkZWZhdWx0OiBudWxsLFxyXG4gICAgICAgIHR5cGU6IGNjLk5vZGVcclxuICAgICAgfSxcclxuICAgICAgY29udHJvbEhpbnRMYWJlbDoge1xyXG4gICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgdHlwZTogY2MuTGFiZWxcclxuICAgICAgfSxcclxuICAgICAga2V5Ym9hcmRIaW50OiB7XHJcbiAgICAgICAgZGVmYXVsdDogJycsXHJcbiAgICAgICAgbXVsdGlsaW5lOiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIHRvdWNoSGludDoge1xyXG4gICAgICAgIGRlZmF1bHQ6ICcnLFxyXG4gICAgICAgIG11bHRpbGluZTogdHJ1ZVxyXG4gICAgICB9LFxyXG4gICAgfSxcclxuXHJcbiAgICAvLyBMSUZFLUNZQ0xFIENBTExCQUNLUzpcclxuXHJcbiAgICBvbkxvYWQgKCkge1xyXG4gICAgICAgIC8vIOiOt+WPluWcsOW5s+mdoueahHnovbTlnZDmoIdcclxuICAgICAgICB0aGlzLmdyb3VuZFkgPSB0aGlzLmdyb3VuZC55ICsgdGhpcy5ncm91bmQuaGVpZ2h0LzI7XHJcbiAgICAgICAgLy8gc3RvcmUgbGFzdCBzdGFyJ3MgeCBwb3NpdGlvblxyXG4gICAgICAgIHRoaXMuY3VycmVudFN0YXIgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFN0YXJYID0gMDtcclxuXHJcbiAgICAgICAgLy8g5Yid5aeL5YyW6K6h5pe25ZmoXHJcbiAgICAgICAgdGhpcy50aW1lciA9IDA7XHJcbiAgICAgICAgdGhpcy5zdGFyRHVyYXRpb24gPSAwO1xyXG5cclxuICAgICAgICAvLyBpcyBzaG93aW5nIG1lbnUgb3IgcnVubmluZyBnYW1lXHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gZmFsc2U7XHJcbiAgICAgICAgLy8gaW5pdGlhbGl6ZSBjb250cm9sIGhpbnRcclxuICAgICAgICB2YXIgaGludFRleHQgPSBjYy5zeXMuaXNNb2JpbGUgPyB0aGlzLnRvdWNoSGludCA6IHRoaXMua2V5Ym9hcmRIaW50O1xyXG4gICAgICAgIHRoaXMuY29udHJvbEhpbnRMYWJlbC5zdHJpbmcgPSBoaW50VGV4dDtcclxuXHJcbiAgICAgICAgLy8gaW5pdGlhbGl6ZSBzdGFyIGFuZCBzY29yZSBwb29sXHJcbiAgICAgICAgdGhpcy5zdGFyUG9vbCA9IG5ldyBjYy5Ob2RlUG9vbCgnU3RhcicpO1xyXG4gICAgICAgIHRoaXMuc2NvcmVQb29sID0gbmV3IGNjLk5vZGVQb29sKCdTY29yZUZYJyk7XHJcbiAgICAgICAgXHJcblxyXG4gICAgICAgIFxyXG4gICAgfSxcclxuICAgIG9uU3RhcnRHYW1lICgpIHtcclxuICAgICAgICAvLyDliJ3lp4vljJborqHliIZcclxuICAgICAgICB0aGlzLnJlc2V0U2NvcmUoKTtcclxuICAgICAgICAvLyBzZXQgZ2FtZSBzdGF0ZSB0byBydW5uaW5nXHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICAvLyBzZXQgYnV0dG9uIGFuZCBnYW1lb3ZlciB0ZXh0IG91dCBvZiBzY3JlZW5cclxuICAgICAgICB0aGlzLmJ0bk5vZGUueCA9IDMwMDA7XHJcbiAgICAgICAgdGhpcy5nYW1lT3Zlck5vZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgLy8gcmVzZXQgcGxheWVyIHBvc2l0aW9uIGFuZCBtb3ZlIHNwZWVkXHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuc3RhcnRNb3ZlQXQoY2MudjIoMCwgdGhpcy5ncm91bmRZKSk7XHJcbiAgICAgICAgLy8gc3Bhd24gc3RhclxyXG4gICAgICAgIHRoaXMuc3Bhd25OZXdTdGFyKCk7XHJcbiAgICB9LFxyXG4gICAgc3Bhd25OZXdTdGFyKCkge1xyXG4gICAgICAgIHZhciBuZXdTdGFyID0gbnVsbDtcclxuICAgICAgICAvLyDkvb/nlKjnu5nlrprnmoTmqKHmnb/lnKjlnLrmma/kuK3nlJ/miJDkuIDkuKrmlrDoioLngrlcclxuICAgICAgICBpZiAodGhpcy5zdGFyUG9vbC5zaXplKCkgPiAwKSB7XHJcbiAgICAgICAgICAgIG5ld1N0YXIgPSB0aGlzLnN0YXJQb29sLmdldCh0aGlzKTsgLy8gdGhpcyB3aWxsIGJlIHBhc3NlZCB0byBTdGFyJ3MgcmV1c2UgbWV0aG9kXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbmV3U3RhciA9IGNjLmluc3RhbnRpYXRlKHRoaXMuc3RhclByZWZhYik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIOWwhuaWsOWinueahOiKgueCuea3u+WKoOWIsENhbnZhc+iKgueCueS4i+mdolxyXG4gICAgICAgIHRoaXMubm9kZS5hZGRDaGlsZChuZXdTdGFyKTtcclxuICAgICAgICAvLyDkuLrmmJ/mmJ/orr7nva7kuIDkuKrpmo/mnLrkvY3nva5cclxuICAgICAgICBuZXdTdGFyLnNldFBvc2l0aW9uKHRoaXMuZ2V0TmV3U3RhclBvc2l0aW9uKCkpXHJcbiAgICAgICAgLy8g5Zyo5pif5pif6ISa5pys57uE5Lu25LiK5L+d5a2YR2FtZeWvueixoeeahOW8leeUqFxyXG4gICAgICAgIG5ld1N0YXIuZ2V0Q29tcG9uZW50KCdTdGFyJykuaW5pdCh0aGlzKTtcclxuICAgICAgICAvLyBzdGFydCBzdGFyIHRpbWVyIGFuZCBzdG9yZSBzdGFyIHJlZmVyZW5jZVxyXG4gICAgICAgIHRoaXMuc3RhcnRUaW1lcigpO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFN0YXIgPSBuZXdTdGFyO1xyXG4gICAgfSxcclxuICAgIGRlc3Bhd25TdGFyIChzdGFyKSB7XHJcbiAgICAgICAgdGhpcy5zdGFyUG9vbC5wdXQoc3Rhcik7XHJcbiAgICAgICAgdGhpcy5zcGF3bk5ld1N0YXIoKTtcclxuICAgIH0sXHJcbiAgICBzdGFydFRpbWVyICgpIHtcclxuICAgICAgICAvLyBnZXQgYSBsaWZlIGR1cmF0aW9uIGZvciBuZXh0IHN0YXJcclxuICAgICAgICB0aGlzLnN0YXJEdXJhdGlvbiA9IHRoaXMubWluU3RhckR1cmF0aW9uICsgTWF0aC5yYW5kb20oKSAqICh0aGlzLm1heFN0YXJEdXJhdGlvbiAtIHRoaXMubWluU3RhckR1cmF0aW9uKTtcclxuICAgICAgICB0aGlzLnRpbWVyID0gMDtcclxuICAgIH0sXHJcbiAgICBnZXROZXdTdGFyUG9zaXRpb24oKSB7XHJcbiAgICAgICAgLy8gaWYgdGhlcmUncyBubyBzdGFyLCBzZXQgYSByYW5kb20geCBwb3NcclxuICAgICAgICBpZiAoIXRoaXMuY3VycmVudFN0YXIpIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50U3RhclggPSAoTWF0aC5yYW5kb20oKSAtIDAuNSkgKiAyICogdGhpcy5ub2RlLndpZHRoLzI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciByYW5kWCA9IDA7XHJcbiAgICAgICAgLy8g5qC55o2u5Zyw5bmz6Z2i5L2N572u5ZKM5Li76KeS6Lez6LeD6auY5bqm77yM6ZqP5py65b6X5Yiw5LiA5Liq5pif5pif55qEeeWdkOagh1xyXG4gICAgICAgIHZhciByYW5kWSA9IHRoaXMuZ3JvdW5kWSArIE1hdGgucmFuZG9tKCkgKiB0aGlzLnBsYXllci5nZXRDb21wb25lbnQoJ1BsYXllcicpLmp1bXBIZWlnaHQgKyA1MDtcclxuICAgICAgICAvLyDmoLnmja7lsY/luZXlrr3luqbvvIzpmo/mnLrlvpfliLDkuIDkuKrmmJ/mmJ945Z2Q5qCHXHJcbiAgICAgICAgdmFyIG1heFggPSB0aGlzLm5vZGUud2lkdGgvMjtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50U3RhclggPj0gMCkge1xyXG4gICAgICAgICAgICByYW5kWCA9IC1NYXRoLnJhbmRvbSgpICogbWF4WDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByYW5kWCA9IE1hdGgucmFuZG9tKCkgKiBtYXhYO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmN1cnJlbnRTdGFyWCA9IHJhbmRYO1xyXG4gICAgICAgIC8vIOi/lOWbnuaYn+aYn+WdkOagh1xyXG4gICAgICAgIHJldHVybiBjYy52MihyYW5kWCwgcmFuZFkpO1xyXG4gICAgfSxcclxuICAgIGdhaW5TY29yZShwb3MpIHtcclxuICAgICAgICB0aGlzLnNjb3JlICs9IDE7XHJcbiAgICAgICAgLy8g5pu05pawIHNjb3JlRGlzcGxheSBMYWJlbCDnmoTmloflrZdcclxuICAgICAgICB0aGlzLnNjb3JlRGlzcGxheS5zdHJpbmcgPSAnU2NvcmU6ICcgKyB0aGlzLnNjb3JlLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgLy8g5pKt5pS+54m55pWIXHJcbiAgICAgICAgdmFyIGZ4ID0gdGhpcy5zcGF3blNjb3JlRlgoKTtcclxuICAgICAgICB0aGlzLm5vZGUuYWRkQ2hpbGQoZngubm9kZSk7XHJcbiAgICAgICAgZngubm9kZS5zZXRQb3NpdGlvbihwb3MpO1xyXG4gICAgICAgIGZ4LnBsYXkoKTtcclxuICAgICAgICAvLyDmkq3mlL7lvpfliIbpn7PmlYhcclxuICAgICAgICBjYy5hdWRpb0VuZ2luZS5wbGF5RWZmZWN0KHRoaXMuc2NvcmVBdWRpbywgZmFsc2UpO1xyXG4gICAgfSxcclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICAvLyDmr4/luKfmm7TmlrDorqHml7blmajvvIzotoXov4fpmZDluqbov5jmsqHmnInnlJ/miJDmlrDnmoTmmJ/mmJ9cclxuICAgICAgICAvLyDlsLHkvJrosIPnlKjmuLjmiI/lpLHotKXpgLvovpFcclxuICAgICAgICBpZiAodGhpcy50aW1lciA+IHRoaXMuc3RhckR1cmF0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2FtZU92ZXIoKTtcclxuICAgICAgICAgICAgdGhpcy5lbmFibGVkID0gZmFsc2U7ICAgLy8gZGlzYWJsZSB0aGlzIHRvIGF2b2lkIGdhbWVPdmVyKCkgcmVwZWF0ZWRseVxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudGltZXIgKz0gZHQ7XHJcbiAgICB9LFxyXG4gICAgcmVzZXRTY29yZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHRoaXMuc2NvcmUgPSAwO1xyXG4gICAgICAgIHRoaXMuc2NvcmVEaXNwbGF5LnN0cmluZyA9ICdTY29yZTogJyArIHRoaXMuc2NvcmUudG9TdHJpbmcoKTtcclxuICAgIH0sXHJcbiAgICBzcGF3blNjb3JlRlggKCkge1xyXG4gICAgICAgIHZhciBmeDtcclxuICAgICAgICBpZiAodGhpcy5zY29yZVBvb2wuc2l6ZSgpID4gMCkge1xyXG4gICAgICAgICAgICBmeCA9IHRoaXMuc2NvcmVQb29sLmdldCgpO1xyXG4gICAgICAgICAgICByZXR1cm4gZnguZ2V0Q29tcG9uZW50KCdTY29yZUZYJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZnggPSBjYy5pbnN0YW50aWF0ZSh0aGlzLnNjb3JlRlhQcmVmYWIpLmdldENvbXBvbmVudCgnU2NvcmVGWCcpO1xyXG4gICAgICAgICAgICBmeC5pbml0KHRoaXMpO1xyXG4gICAgICAgICAgICByZXR1cm4gZng7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIGRlc3Bhd25TY29yZUZYIChzY29yZUZYKSB7XHJcbiAgICAgICAgdGhpcy5zY29yZVBvb2wucHV0KHNjb3JlRlgpO1xyXG4gICAgfSxcclxuICAgIGdhbWVPdmVyKCkge1xyXG4gICAgICAgIHRoaXMuZ2FtZU92ZXJOb2RlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXIuZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMucGxheWVyLnN0b3BNb3ZlKCk7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50U3Rhci5kZXN0cm95KCk7XHJcbiAgICAgICAgdGhpcy5idG5Ob2RlLnggPSAwO1xyXG4gICAgfVxyXG59KTtcclxuIl19