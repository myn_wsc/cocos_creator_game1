
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/ScoreFX.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '53983Bn/u1AxLjU2OZoATPB', 'ScoreFX');
// scripts/ScoreFX.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    anim: {
      "default": null,
      type: cc.Animation
    }
  },
  init: function init(game) {
    this.game = game;
    this.anim.getComponent('ScoreAnim').init(this);
  },
  despawn: function despawn() {
    this.game.despawnScoreFX(this.node);
  },
  play: function play() {
    this.anim.play('score_pop');
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU2NvcmVGWC5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImFuaW0iLCJ0eXBlIiwiQW5pbWF0aW9uIiwiaW5pdCIsImdhbWUiLCJnZXRDb21wb25lbnQiLCJkZXNwYXduIiwiZGVzcGF3blNjb3JlRlgiLCJub2RlIiwicGxheSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLElBQUksRUFBRTtBQUNGLGlCQUFTLElBRFA7QUFFRkMsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNNO0FBRlA7QUFERSxHQUhQO0FBVUxDLEVBQUFBLElBVkssZ0JBVUFDLElBVkEsRUFVTTtBQUNQLFNBQUtBLElBQUwsR0FBWUEsSUFBWjtBQUNBLFNBQUtKLElBQUwsQ0FBVUssWUFBVixDQUF1QixXQUF2QixFQUFvQ0YsSUFBcEMsQ0FBeUMsSUFBekM7QUFDSCxHQWJJO0FBY0xHLEVBQUFBLE9BZEsscUJBY0s7QUFDTixTQUFLRixJQUFMLENBQVVHLGNBQVYsQ0FBeUIsS0FBS0MsSUFBOUI7QUFDSCxHQWhCSTtBQWlCTEMsRUFBQUEsSUFqQkssa0JBaUJFO0FBQ0gsU0FBS1QsSUFBTCxDQUFVUyxJQUFWLENBQWUsV0FBZjtBQUNIO0FBbkJJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9jbGFzcy5odG1sXHJcbi8vIExlYXJuIEF0dHJpYnV0ZTpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICBhbmltOiB7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXHJcbiAgICAgICAgICAgIHR5cGU6IGNjLkFuaW1hdGlvblxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgaW5pdChnYW1lKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lID0gZ2FtZTtcclxuICAgICAgICB0aGlzLmFuaW0uZ2V0Q29tcG9uZW50KCdTY29yZUFuaW0nKS5pbml0KHRoaXMpO1xyXG4gICAgfSxcclxuICAgIGRlc3Bhd24oKSB7XHJcbiAgICAgICAgdGhpcy5nYW1lLmRlc3Bhd25TY29yZUZYKHRoaXMubm9kZSk7XHJcbiAgICB9LFxyXG4gICAgcGxheSgpIHtcclxuICAgICAgICB0aGlzLmFuaW0ucGxheSgnc2NvcmVfcG9wJyk7XHJcbiAgICB9XHJcbn0pO1xyXG4iXX0=