
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Star.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'b172bu65n9NXaXmZQ2uIIVh', 'Star');
// scripts/Star.js

"use strict";

// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html
cc.Class({
  "extends": cc.Component,
  properties: {
    // 星星和主角之间的距离小于这个数值时，就会完成收集
    pickRadius: 0
  },
  getPlayerDistance: function getPlayerDistance() {
    // 根据Player节点位置判断距离
    var playerPos = this.game.player.getCenterPos(); // 根据两点位置计算两点之间距离

    var dist = this.node.position.sub(playerPos).mag();
    return dist;
  },
  onPicked: function onPicked() {
    var pos = this.node.getPosition(); // 调用 Game 脚本的得分方法

    this.game.gainScore(pos); // 当星星被收集时，调用 Game 脚本中的接口，销毁当前星星节点，生成一个新的星星

    this.game.despawnStar(this.node);
  },
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    this.enabled = false;
  },
  // use this for initialization
  init: function init(game) {
    this.game = game;
    this.enabled = true;
    this.node.opacity = 255;
  },
  reuse: function reuse(game) {
    this.init(game);
  },
  start: function start() {},
  update: function update(dt) {
    // 每帧判断星星和主角之间的距离是否小于收集距离
    if (this.getPlayerDistance() < this.pickRadius) {
      // 调用收集行为
      this.onPicked();
      return;
    } // 根据Game脚本中的计时器更新星星的透明度


    var opacityRatio = 1 - this.game.timer / this.game.starDuration;
    var minOpcity = 50;
    this.node.opacity = minOpcity + Math.floor(opacityRatio * (255 - minOpcity));
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU3Rhci5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsInBpY2tSYWRpdXMiLCJnZXRQbGF5ZXJEaXN0YW5jZSIsInBsYXllclBvcyIsImdhbWUiLCJwbGF5ZXIiLCJnZXRDZW50ZXJQb3MiLCJkaXN0Iiwibm9kZSIsInBvc2l0aW9uIiwic3ViIiwibWFnIiwib25QaWNrZWQiLCJwb3MiLCJnZXRQb3NpdGlvbiIsImdhaW5TY29yZSIsImRlc3Bhd25TdGFyIiwib25Mb2FkIiwiZW5hYmxlZCIsImluaXQiLCJvcGFjaXR5IiwicmV1c2UiLCJzdGFydCIsInVwZGF0ZSIsImR0Iiwib3BhY2l0eVJhdGlvIiwidGltZXIiLCJzdGFyRHVyYXRpb24iLCJtaW5PcGNpdHkiLCJNYXRoIiwiZmxvb3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSO0FBQ0FDLElBQUFBLFVBQVUsRUFBRTtBQUZKLEdBSFA7QUFPTEMsRUFBQUEsaUJBUEssK0JBT2U7QUFDaEI7QUFDQSxRQUFJQyxTQUFTLEdBQUcsS0FBS0MsSUFBTCxDQUFVQyxNQUFWLENBQWlCQyxZQUFqQixFQUFoQixDQUZnQixDQUdoQjs7QUFDQSxRQUFJQyxJQUFJLEdBQUcsS0FBS0MsSUFBTCxDQUFVQyxRQUFWLENBQW1CQyxHQUFuQixDQUF1QlAsU0FBdkIsRUFBa0NRLEdBQWxDLEVBQVg7QUFDQSxXQUFPSixJQUFQO0FBQ0gsR0FiSTtBQWNMSyxFQUFBQSxRQWRLLHNCQWNNO0FBQ1AsUUFBSUMsR0FBRyxHQUFHLEtBQUtMLElBQUwsQ0FBVU0sV0FBVixFQUFWLENBRE8sQ0FFUDs7QUFDQSxTQUFLVixJQUFMLENBQVVXLFNBQVYsQ0FBb0JGLEdBQXBCLEVBSE8sQ0FJUDs7QUFDQSxTQUFLVCxJQUFMLENBQVVZLFdBQVYsQ0FBc0IsS0FBS1IsSUFBM0I7QUFDSCxHQXBCSTtBQXFCTDtBQUVBUyxFQUFBQSxNQXZCSyxvQkF1Qks7QUFDTixTQUFLQyxPQUFMLEdBQWUsS0FBZjtBQUNILEdBekJJO0FBMEJMO0FBQ0FDLEVBQUFBLElBM0JLLGdCQTJCQ2YsSUEzQkQsRUEyQk87QUFDUixTQUFLQSxJQUFMLEdBQVlBLElBQVo7QUFDQSxTQUFLYyxPQUFMLEdBQWUsSUFBZjtBQUNBLFNBQUtWLElBQUwsQ0FBVVksT0FBVixHQUFvQixHQUFwQjtBQUNILEdBL0JJO0FBaUNMQyxFQUFBQSxLQWpDSyxpQkFpQ0VqQixJQWpDRixFQWlDUTtBQUNULFNBQUtlLElBQUwsQ0FBVWYsSUFBVjtBQUNILEdBbkNJO0FBb0NMa0IsRUFBQUEsS0FwQ0ssbUJBb0NJLENBRVIsQ0F0Q0k7QUF3Q0xDLEVBQUFBLE1BeENLLGtCQXdDR0MsRUF4Q0gsRUF3Q087QUFDUjtBQUNBLFFBQUksS0FBS3RCLGlCQUFMLEtBQTJCLEtBQUtELFVBQXBDLEVBQWdEO0FBQzVDO0FBQ0EsV0FBS1csUUFBTDtBQUNBO0FBQ0gsS0FOTyxDQU9SOzs7QUFDQSxRQUFJYSxZQUFZLEdBQUcsSUFBSSxLQUFLckIsSUFBTCxDQUFVc0IsS0FBVixHQUFnQixLQUFLdEIsSUFBTCxDQUFVdUIsWUFBakQ7QUFDQSxRQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQSxTQUFLcEIsSUFBTCxDQUFVWSxPQUFWLEdBQW9CUSxTQUFTLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXTCxZQUFZLElBQUksTUFBTUcsU0FBVixDQUF2QixDQUFoQztBQUNIO0FBbkRJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIi8vIExlYXJuIGNjLkNsYXNzOlxyXG4vLyAgLSBodHRwczovL2RvY3MuY29jb3MuY29tL2NyZWF0b3IvbWFudWFsL2VuL3NjcmlwdGluZy9jbGFzcy5odG1sXHJcbi8vIExlYXJuIEF0dHJpYnV0ZTpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvcmVmZXJlbmNlL2F0dHJpYnV0ZXMuaHRtbFxyXG4vLyBMZWFybiBsaWZlLWN5Y2xlIGNhbGxiYWNrczpcclxuLy8gIC0gaHR0cHM6Ly9kb2NzLmNvY29zLmNvbS9jcmVhdG9yL21hbnVhbC9lbi9zY3JpcHRpbmcvbGlmZS1jeWNsZS1jYWxsYmFja3MuaHRtbFxyXG5cclxuY2MuQ2xhc3Moe1xyXG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxyXG5cclxuICAgIHByb3BlcnRpZXM6IHtcclxuICAgICAgICAvLyDmmJ/mmJ/lkozkuLvop5LkuYvpl7TnmoTot53nprvlsI/kuo7ov5nkuKrmlbDlgLzml7bvvIzlsLHkvJrlrozmiJDmlLbpm4ZcclxuICAgICAgICBwaWNrUmFkaXVzOiAwLFxyXG4gICAgfSxcclxuICAgIGdldFBsYXllckRpc3RhbmNlKCkge1xyXG4gICAgICAgIC8vIOagueaNrlBsYXllcuiKgueCueS9jee9ruWIpOaWrei3neemu1xyXG4gICAgICAgIHZhciBwbGF5ZXJQb3MgPSB0aGlzLmdhbWUucGxheWVyLmdldENlbnRlclBvcygpO1xyXG4gICAgICAgIC8vIOagueaNruS4pOeCueS9jee9ruiuoeeul+S4pOeCueS5i+mXtOi3neemu1xyXG4gICAgICAgIHZhciBkaXN0ID0gdGhpcy5ub2RlLnBvc2l0aW9uLnN1YihwbGF5ZXJQb3MpLm1hZygpO1xyXG4gICAgICAgIHJldHVybiBkaXN0O1xyXG4gICAgfSxcclxuICAgIG9uUGlja2VkKCkge1xyXG4gICAgICAgIHZhciBwb3MgPSB0aGlzLm5vZGUuZ2V0UG9zaXRpb24oKTtcclxuICAgICAgICAvLyDosIPnlKggR2FtZSDohJrmnKznmoTlvpfliIbmlrnms5VcclxuICAgICAgICB0aGlzLmdhbWUuZ2FpblNjb3JlKHBvcyk7XHJcbiAgICAgICAgLy8g5b2T5pif5pif6KKr5pS26ZuG5pe277yM6LCD55SoIEdhbWUg6ISa5pys5Lit55qE5o6l5Y+j77yM6ZSA5q+B5b2T5YmN5pif5pif6IqC54K577yM55Sf5oiQ5LiA5Liq5paw55qE5pif5pifXHJcbiAgICAgICAgdGhpcy5nYW1lLmRlc3Bhd25TdGFyKHRoaXMubm9kZSk7XHJcbiAgICB9LFxyXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XHJcblxyXG4gICAgb25Mb2FkICgpIHtcclxuICAgICAgICB0aGlzLmVuYWJsZWQgPSBmYWxzZTtcclxuICAgIH0sXHJcbiAgICAvLyB1c2UgdGhpcyBmb3IgaW5pdGlhbGl6YXRpb25cclxuICAgIGluaXQgKGdhbWUpIHtcclxuICAgICAgICB0aGlzLmdhbWUgPSBnYW1lO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPSAyNTU7XHJcbiAgICB9LFxyXG5cclxuICAgIHJldXNlIChnYW1lKSB7XHJcbiAgICAgICAgdGhpcy5pbml0KGdhbWUpO1xyXG4gICAgfSxcclxuICAgIHN0YXJ0ICgpIHtcclxuXHJcbiAgICB9LFxyXG5cclxuICAgIHVwZGF0ZSAoZHQpIHtcclxuICAgICAgICAvLyDmr4/luKfliKTmlq3mmJ/mmJ/lkozkuLvop5LkuYvpl7TnmoTot53nprvmmK/lkKblsI/kuo7mlLbpm4bot53nprtcclxuICAgICAgICBpZiAodGhpcy5nZXRQbGF5ZXJEaXN0YW5jZSgpIDwgdGhpcy5waWNrUmFkaXVzKSB7XHJcbiAgICAgICAgICAgIC8vIOiwg+eUqOaUtumbhuihjOS4ulxyXG4gICAgICAgICAgICB0aGlzLm9uUGlja2VkKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8g5qC55o2uR2FtZeiEmuacrOS4reeahOiuoeaXtuWZqOabtOaWsOaYn+aYn+eahOmAj+aYjuW6plxyXG4gICAgICAgIHZhciBvcGFjaXR5UmF0aW8gPSAxIC0gdGhpcy5nYW1lLnRpbWVyL3RoaXMuZ2FtZS5zdGFyRHVyYXRpb247XHJcbiAgICAgICAgdmFyIG1pbk9wY2l0eSA9IDUwO1xyXG4gICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gbWluT3BjaXR5ICsgTWF0aC5mbG9vcihvcGFjaXR5UmF0aW8gKiAoMjU1IC0gbWluT3BjaXR5KSk7XHJcbiAgICB9LFxyXG59KTtcclxuIl19